(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["office-ui-fabric-vue"] = factory();
	else
		root["office-ui-fabric-vue"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fae3");
/******/ })
/************************************************************************/
/******/ ({

/***/ "00db":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OverflowSet_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("c5b1");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OverflowSet_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OverflowSet_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_OverflowSet_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "01bf":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("e3cb");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("7f2d97b0", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "0d58":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__("ce10");
var enumBugKeys = __webpack_require__("e11e");

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};


/***/ }),

/***/ "11e9":
/***/ (function(module, exports, __webpack_require__) {

var pIE = __webpack_require__("52a7");
var createDesc = __webpack_require__("4630");
var toIObject = __webpack_require__("6821");
var toPrimitive = __webpack_require__("6a99");
var has = __webpack_require__("69a8");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__("9e1e") ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) { /* empty */ }
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};


/***/ }),

/***/ "12fb":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_fc1d4600_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("01bf");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_fc1d4600_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_fc1d4600_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Checkbox_vue_vue_type_style_index_0_id_fc1d4600_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "135b":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Spinner_vue_vue_type_style_index_0_id_40312040_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("d444");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Spinner_vue_vue_type_style_index_0_id_40312040_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Spinner_vue_vue_type_style_index_0_id_40312040_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Spinner_vue_vue_type_style_index_0_id_40312040_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1495":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var anObject = __webpack_require__("cb7c");
var getKeys = __webpack_require__("0d58");

module.exports = __webpack_require__("9e1e") ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) dP.f(O, P = keys[i++], Properties[P]);
  return O;
};


/***/ }),

/***/ "1bb5":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommandBar_vue_vue_type_style_index_0_id_073452f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("7028");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommandBar_vue_vue_type_style_index_0_id_073452f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommandBar_vue_vue_type_style_index_0_id_073452f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CommandBar_vue_vue_type_style_index_0_id_073452f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1c34":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_986803ca_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("9a6e");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_986803ca_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_986803ca_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Toggle_vue_vue_type_style_index_0_id_986803ca_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "1c6b":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("2b28");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("751d6562", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "1eb2":
/***/ (function(module, exports, __webpack_require__) {

// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var i
  if ((i = window.document.currentScript) && (i = i.src.match(/(.+\/)[^/]+\.js$/))) {
    __webpack_require__.p = i[1] // eslint-disable-line
  }
}


/***/ }),

/***/ "214f":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var fails = __webpack_require__("79e5");
var defined = __webpack_require__("be13");
var wks = __webpack_require__("2b4c");

module.exports = function (KEY, length, exec) {
  var SYMBOL = wks(KEY);
  var fns = exec(defined, SYMBOL, ''[KEY]);
  var strfn = fns[0];
  var rxfn = fns[1];
  if (fails(function () {
    var O = {};
    O[SYMBOL] = function () { return 7; };
    return ''[KEY](O) != 7;
  })) {
    redefine(String.prototype, KEY, strfn);
    hide(RegExp.prototype, SYMBOL, length == 2
      // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
      // 21.2.5.11 RegExp.prototype[@@split](string, limit)
      ? function (string, arg) { return rxfn.call(string, this, arg); }
      // 21.2.5.6 RegExp.prototype[@@match](string)
      // 21.2.5.9 RegExp.prototype[@@search](string)
      : function (string) { return rxfn.call(string, this); }
    );
  }
};


/***/ }),

/***/ "230e":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var document = __webpack_require__("7726").document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};


/***/ }),

/***/ "2350":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "286a":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\ninput.root[data-v-b28a83c4],textarea.root[data-v-b28a83c4]{box-shadow:none;box-sizing:border-box;margin:0;padding:0;position:relative\n}\n.screenReaderOnly[data-v-b28a83c4]{border:0;clip:rect(0,0,0,0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px\n}\n.fieldGroup[data-v-b28a83c4]{align-items:stretch;background:#fff;border:1px solid #a6a6a6;box-shadow:none;box-sizing:border-box;display:flex;flex-direction:row;height:32px;margin:0;padding:0;position:relative\n}\n.fieldGroup[data-v-b28a83c4]:hover{border-color:#212121\n}\n.fieldGroup.fieldGroupIsFocused[data-v-b28a83c4]{border-color:#0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.fieldGroup.fieldGroupIsFocused[data-v-b28a83c4]{border-width:2px\n}\n.fieldGroup.fieldGroupIsFocused .field[data-v-b28a83c4],[dir=rtl] .fieldGroup.fieldGroupIsFocused .field[data-v-b28a83c4]{padding:0 11px\n}\n}\n.fieldGroup.fieldGroupIsFocused.invalid[data-v-b28a83c4]{border-color:#a80000\n}\n.rootIsDisabled .fieldGroup[data-v-b28a83c4]{background-color:#f4f4f4;border-color:#f4f4f4\n}\n@media screen and (-ms-high-contrast:active){\n.fieldGroup.fieldGroupIsFocused[data-v-b28a83c4],.fieldGroup[data-v-b28a83c4]:hover{border-color:Highlight\n}\n}\n.fieldGroup[data-v-b28a83c4]::-ms-clear{display:none\n}\n.fieldGroup[data-v-b28a83c4] :-ms-input-placeholder,.fieldGroup[data-v-b28a83c4] ::placeholder{color:#666;opacity:1\n}\n.root.rootIsDisabled[data-v-b28a83c4] .field{background-color:#f4f4f4;border-color:#f4f4f4\n}\n.fieldPrefixSuffix[data-v-b28a83c4]{align-items:center;background:#f4f4f4;color:#666;display:flex;line-height:1;padding:0 10px;white-space:nowrap\n}\n.field[data-v-b28a83c4]{background:none;background-color:transparent;border:none;border-radius:0;box-shadow:none;box-sizing:border-box;color:#333;font-size:14px;margin:0;min-width:0;outline:0;text-overflow:ellipsis;width:100%\n}\n.field[data-v-b28a83c4],[dir=rtl] .field[data-v-b28a83c4]{padding:0 12px\n}\n.field[data-v-b28a83c4]:active,.field[data-v-b28a83c4]:focus,.field[data-v-b28a83c4]:hover{outline:0\n}\n[dir=ltr] .field.hasIcon[data-v-b28a83c4]{padding-right:24px\n}\n[dir=rtl] .field.hasIcon[data-v-b28a83c4]{padding-left:24px\n}\n.field[disabled][data-v-b28a83c4]{background-color:transparent;border-color:transparent\n}\n.field[data-v-b28a83c4]::placeholder{color:#666\n}\n.root.rootIsRequiredLabel[data-v-b28a83c4] .ms-Label:after{color:#a80000;content:\" *\"\n}\n.root.rootIsRequiredPlaceholderOnly[data-v-b28a83c4] .ms-TextField-fieldGroup:after{color:#a80000;content:\"*\";position:absolute;top:-5px\n}\n[dir=ltr] .root.rootIsRequiredPlaceholderOnly[data-v-b28a83c4] .ms-TextField-fieldGroup:after{right:-10px\n}\n[dir=rtl] .root.rootIsRequiredPlaceholderOnly[data-v-b28a83c4] .ms-TextField-fieldGroup:after{left:-10px\n}\n.root.rootIsActive[data-v-b28a83c4]{border-color:#0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.root.rootIsActive[data-v-b28a83c4]{border-width:2px\n}\n.root.rootIsActive .field[data-v-b28a83c4],[dir=rtl] .root.rootIsActive .field[data-v-b28a83c4]{padding:0 11px\n}\n}\n.icon[data-v-b28a83c4]{bottom:5px;font-size:16px;line-height:18px;pointer-events:none;position:absolute;top:auto\n}\nhtml[dir=ltr] .icon[data-v-b28a83c4]{right:8px\n}\nhtml[dir=rtl] .icon[data-v-b28a83c4]{left:8px\n}\n.description[data-v-b28a83c4]{color:#666;font-size:11px\n}\n.rootIsBorderless .fieldGroup[data-v-b28a83c4]{border-color:transparent;border-width:0\n}\n.root.rootIsUnderlined[data-v-b28a83c4]{border:0 solid #a6a6a6\n}\n.root.rootIsUnderlined .wrapper[data-v-b28a83c4]{border-bottom-color:inherit;border-bottom-style:solid;border-bottom-width:1px;display:flex;width:100%\n}\n.root.rootIsUnderlined .wrapper.invalid[data-v-b28a83c4],.root.rootIsUnderlined .wrapper.invalid[data-v-b28a83c4]:focus,.root.rootIsUnderlined .wrapper.invalid[data-v-b28a83c4]:hover{border-bottom:1px solid #a80000\n}\n.root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{font-size:14px;height:32px;line-height:22px\n}\n[dir=ltr] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{margin-right:8px\n}\n[dir=rtl] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{margin-left:8px\n}\n.root.rootIsUnderlined .fieldGroup[data-v-b28a83c4]{border-width:0;flex:1 1 0px\n}\n[dir=ltr] .root.rootIsUnderlined .fieldGroup[data-v-b28a83c4]{text-align:left\n}\n[dir=rtl] .root.rootIsUnderlined .fieldGroup[data-v-b28a83c4]{text-align:right\n}\n.root.rootIsUnderlined.rootIsDisabled[data-v-b28a83c4]{border-color:#f4f4f4\n}\n.root.rootIsUnderlined.rootIsDisabled[data-v-b28a83c4] .ms-Label{color:#a6a6a6\n}\n.root.rootIsUnderlined.rootIsDisabled .field[data-v-b28a83c4]{background-color:transparent;color:#a6a6a6\n}\n.root.rootIsUnderlined.rootIsDisabled .fieldGroup[data-v-b28a83c4]{background-color:transparent\n}\n.root.rootIsUnderlined[data-v-b28a83c4]:hover:not(.rootIsActive):not(.rootIsDisabled){border-color:#212121\n}\n.root.rootIsUnderlined.rootIsActive[data-v-b28a83c4]{border-color:#0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.root.rootIsUnderlined.rootIsActive[data-v-b28a83c4]{border-width:2px\n}\n.root.rootIsUnderlined.rootIsActive .field[data-v-b28a83c4],[dir=rtl] .root.rootIsUnderlined.rootIsActive .field[data-v-b28a83c4]{padding:0 11px\n}\n}\n@media screen and (-ms-high-contrast:active){\n.root.rootIsUnderlined.rootIsActive .wrapper[data-v-b28a83c4],.root.rootIsUnderlined:hover:not(.rootIsDisabled) .wrapper[data-v-b28a83c4]{border-color:Highlight\n}\n}\n.root.rootIsMultiline .fieldGroup[data-v-b28a83c4]{display:flex;height:auto;min-height:60px\n}\n.root.rootIsMultiline .field[data-v-b28a83c4]{flex-grow:1;line-height:17px;overflow:auto;padding-top:6px;width:100%\n}\n[dir=ltr] .root.rootIsMultiline .field.hasIcon[data-v-b28a83c4]{padding-right:40px\n}\n[dir=rtl] .root.rootIsMultiline .field.hasIcon[data-v-b28a83c4]{padding-left:40px\n}\n.errorMessage[data-v-b28a83c4]{align-items:center;color:#a80000;display:flex;font-size:12px;font-weight:400;margin:0;padding-top:5px\n}\n.invalid[data-v-b28a83c4],.invalid[data-v-b28a83c4]:focus,.invalid[data-v-b28a83c4]:hover{border-color:#a80000\n}\n[dir=ltr] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{padding-left:12px\n}\n[dir=rtl] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{padding-right:12px\n}\n[dir=ltr] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{padding-right:0\n}\n[dir=rtl] .root.rootIsUnderlined[data-v-b28a83c4] .ms-Label{padding-left:0\n}\nhtml[dir=ltr] .root.rootIsUnderlined .field[data-v-b28a83c4]{text-align:left\n}\nhtml[dir=rtl] .root.rootIsUnderlined .field[data-v-b28a83c4]{text-align:right\n}\n.root.rootIsMultiline .icon[data-v-b28a83c4]{align-items:flex-end;padding-bottom:8px\n}\n[dir=ltr] .root.rootIsMultiline .icon[data-v-b28a83c4]{padding-right:24px\n}\n[dir=rtl] .root.rootIsMultiline .icon[data-v-b28a83c4]{padding-left:24px\n}\n.root.rootIsMultiline .field.fieldIsUnresizable[data-v-b28a83c4]{resize:none\n}\n.hidden[data-v-b28a83c4]{display:none\n}", ""]);

// exports


/***/ }),

/***/ "28a5":
/***/ (function(module, exports, __webpack_require__) {

// @@split logic
__webpack_require__("214f")('split', 2, function (defined, SPLIT, $split) {
  'use strict';
  var isRegExp = __webpack_require__("aae3");
  var _split = $split;
  var $push = [].push;
  var $SPLIT = 'split';
  var LENGTH = 'length';
  var LAST_INDEX = 'lastIndex';
  if (
    'abbc'[$SPLIT](/(b)*/)[1] == 'c' ||
    'test'[$SPLIT](/(?:)/, -1)[LENGTH] != 4 ||
    'ab'[$SPLIT](/(?:ab)*/)[LENGTH] != 2 ||
    '.'[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 ||
    '.'[$SPLIT](/()()/)[LENGTH] > 1 ||
    ''[$SPLIT](/.?/)[LENGTH]
  ) {
    var NPCG = /()??/.exec('')[1] === undefined; // nonparticipating capturing group
    // based on es5-shim implementation, need to rework it
    $split = function (separator, limit) {
      var string = String(this);
      if (separator === undefined && limit === 0) return [];
      // If `separator` is not a regex, use native split
      if (!isRegExp(separator)) return _split.call(string, separator, limit);
      var output = [];
      var flags = (separator.ignoreCase ? 'i' : '') +
                  (separator.multiline ? 'm' : '') +
                  (separator.unicode ? 'u' : '') +
                  (separator.sticky ? 'y' : '');
      var lastLastIndex = 0;
      var splitLimit = limit === undefined ? 4294967295 : limit >>> 0;
      // Make `global` and avoid `lastIndex` issues by working with a copy
      var separatorCopy = new RegExp(separator.source, flags + 'g');
      var separator2, match, lastIndex, lastLength, i;
      // Doesn't need flags gy, but they don't hurt
      if (!NPCG) separator2 = new RegExp('^' + separatorCopy.source + '$(?!\\s)', flags);
      while (match = separatorCopy.exec(string)) {
        // `separatorCopy.lastIndex` is not reliable cross-browser
        lastIndex = match.index + match[0][LENGTH];
        if (lastIndex > lastLastIndex) {
          output.push(string.slice(lastLastIndex, match.index));
          // Fix browsers whose `exec` methods don't consistently return `undefined` for NPCG
          // eslint-disable-next-line no-loop-func
          if (!NPCG && match[LENGTH] > 1) match[0].replace(separator2, function () {
            for (i = 1; i < arguments[LENGTH] - 2; i++) if (arguments[i] === undefined) match[i] = undefined;
          });
          if (match[LENGTH] > 1 && match.index < string[LENGTH]) $push.apply(output, match.slice(1));
          lastLength = match[0][LENGTH];
          lastLastIndex = lastIndex;
          if (output[LENGTH] >= splitLimit) break;
        }
        if (separatorCopy[LAST_INDEX] === match.index) separatorCopy[LAST_INDEX]++; // Avoid an infinite loop
      }
      if (lastLastIndex === string[LENGTH]) {
        if (lastLength || !separatorCopy.test('')) output.push('');
      } else output.push(string.slice(lastLastIndex));
      return output[LENGTH] > splitLimit ? output.slice(0, splitLimit) : output;
    };
  // Chakra, V8
  } else if ('0'[$SPLIT](undefined, 0)[LENGTH]) {
    $split = function (separator, limit) {
      return separator === undefined && limit === 0 ? [] : _split.call(this, separator, limit);
    };
  }
  // 21.1.3.17 String.prototype.split(separator, limit)
  return [function split(separator, limit) {
    var O = defined(this);
    var fn = separator == undefined ? undefined : separator[SPLIT];
    return fn !== undefined ? fn.call(separator, O, limit) : $split.call(String(O), separator, limit);
  }, $split];
});


/***/ }),

/***/ "28cc":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.ms-Shimmer-container[data-v-0cff7e00]{height:auto;position:relative\n}\n.ms-ShimmerElementsGroup-root[data-v-0cff7e00]{align-items:center;display:flex;flex-wrap:nowrap\n}", ""]);

// exports


/***/ }),

/***/ "2aba":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var hide = __webpack_require__("32e9");
var has = __webpack_require__("69a8");
var SRC = __webpack_require__("ca5a")('src');
var TO_STRING = 'toString';
var $toString = Function[TO_STRING];
var TPL = ('' + $toString).split(TO_STRING);

__webpack_require__("8378").inspectSource = function (it) {
  return $toString.call(it);
};

(module.exports = function (O, key, val, safe) {
  var isFunction = typeof val == 'function';
  if (isFunction) has(val, 'name') || hide(val, 'name', key);
  if (O[key] === val) return;
  if (isFunction) has(val, SRC) || hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
  if (O === global) {
    O[key] = val;
  } else if (!safe) {
    delete O[key];
    hide(O, key, val);
  } else if (O[key]) {
    O[key] = val;
  } else {
    hide(O, key, val);
  }
// add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
})(Function.prototype, TO_STRING, function toString() {
  return typeof this == 'function' && this[SRC] || $toString.call(this);
});


/***/ }),

/***/ "2aeb":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__("cb7c");
var dPs = __webpack_require__("1495");
var enumBugKeys = __webpack_require__("e11e");
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');
var Empty = function () { /* empty */ };
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var createDict = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__("230e")('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__("fab2").appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  createDict = iframeDocument.F;
  while (i--) delete createDict[PROTOTYPE][enumBugKeys[i]];
  return createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};


/***/ }),

/***/ "2b28":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-54167381]{height:32px;min-width:80px\n}\n.label[data-v-54167381]{font-weight:600;line-height:100%;margin:0 4px\n}", ""]);

// exports


/***/ }),

/***/ "2b4c":
/***/ (function(module, exports, __webpack_require__) {

var store = __webpack_require__("5537")('wks');
var uid = __webpack_require__("ca5a");
var Symbol = __webpack_require__("7726").Symbol;
var USE_SYMBOL = typeof Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] =
    USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : uid)('Symbol.' + name));
};

$exports.store = store;


/***/ }),

/***/ "2d00":
/***/ (function(module, exports) {

module.exports = false;


/***/ }),

/***/ "2d75":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-04588638]{color:#0078d4;margin:0;overflow:inherit;padding:0;text-overflow:inherit\n}\n.isEnabled[data-v-04588638]:focus,.isEnabled[data-v-04588638]:hover{color:#004578\n}\n.isEnabled[data-v-04588638]:active{color:#0078d4\n}\n.isDisabled[data-v-04588638]{color:#a6a6a6;cursor:default;pointer-events:none\n}\nbutton.root[data-v-04588638]{background:none;border:none;cursor:pointer;display:inline;font-size:inherit\n}\nbutton.root[data-v-04588638]::-moz-focus-inner{border:0\n}\nbutton.root[data-v-04588638]{outline:transparent;position:relative\n}\n.ms-Fabric.is-focusVisible button.root[data-v-04588638]:focus:after{border:1px solid #000;bottom:0;content:\"\";left:0;pointer-events:none;position:absolute;right:0;top:0\n}\nhtml[dir=ltr] button.root[data-v-04588638]{text-align:left\n}\nhtml[dir=rtl] button.root[data-v-04588638]{text-align:right\n}\n@media screen and (-ms-high-contrast:active){\nbutton.root[data-v-04588638]{color:Highlight\n}\n}\na.root[data-v-04588638]{text-decoration:none\n}\n.ms-Fabric.is-focusVisible a.root[data-v-04588638]:focus{outline:1px solid #000\n}", ""]);

// exports


/***/ }),

/***/ "2d95":
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};


/***/ }),

/***/ "2e1b":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-3ddee351]{max-width:280px;min-height:72px\n}\n.root .textContainer[data-v-3ddee351]{text-align:left\n}\n.root .textContainer .label[data-v-3ddee351]{margin:0 0 5px\n}\n.root .textContainer .description[data-v-3ddee351]{-webkit-font-smoothing:antialiased;color:#666;font-family:Segoe UI,\"Segoe UI Web (West European)\",-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif;font-size:12px;font-weight:400;line-height:100%\n}\n.ms-Button--primary .textContainer .description[data-v-3ddee351]{color:#fff\n}", ""]);

// exports


/***/ }),

/***/ "32e9":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc");
var createDesc = __webpack_require__("4630");
module.exports = __webpack_require__("9e1e") ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};


/***/ }),

/***/ "38c2":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("bc41");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("341e2347", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "391d":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Breadcrumb_vue_vue_type_style_index_0_id_8c974350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("4fc8");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Breadcrumb_vue_vue_type_style_index_0_id_8c974350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Breadcrumb_vue_vue_type_style_index_0_id_8c974350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Breadcrumb_vue_vue_type_style_index_0_id_8c974350_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "3bf0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Shimmer_vue_vue_type_style_index_0_id_0cff7e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("948b");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Shimmer_vue_vue_type_style_index_0_id_0cff7e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Shimmer_vue_vue_type_style_index_0_id_0cff7e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Shimmer_vue_vue_type_style_index_0_id_0cff7e00_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "3e71":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.is-disabled .label[data-v-5565ffd4]{color:#a6a6a6\n}\n.root[data-v-5565ffd4]{background-color:#f8f8f8;border:1px solid transparent;border-radius:0;box-sizing:border-box;cursor:pointer;display:inline-block;font-size:14px;font-weight:400;height:40px;padding:0 16px;text-align:center;text-decoration:none;user-select:none;vertical-align:top\n}\n.root[data-v-5565ffd4]:focus,.root[data-v-5565ffd4]:hover{outline:transparent\n}\n.root[data-v-5565ffd4]:hover{background-color:#eaeaea\n}\n.root.ms-Button--primary[data-v-5565ffd4]{background-color:#0078d4;color:#fff\n}\n.root.ms-Button--primary[data-v-5565ffd4]:hover{background-color:#106ebe\n}\n.root.is-disabled[data-v-5565ffd4],.root.ms-Button--primary.is-disabled[data-v-5565ffd4]:hover{background-color:#f8f8f8\n}\n.root.is-disabled[data-v-5565ffd4]:hover{cursor:default\n}\n.flexContainer[data-v-5565ffd4]{align-items:center;display:flex;flex-wrap:nowrap;height:100%;justify-content:center\n}\n.textContainer[data-v-5565ffd4]{flex-grow:1\n}\n.label[data-v-5565ffd4]{font-weight:600;line-height:100%;margin:0 4px\n}", ""]);

// exports


/***/ }),

/***/ "4215":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "4588":
/***/ (function(module, exports) {

// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};


/***/ }),

/***/ "4630":
/***/ (function(module, exports) {

module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};


/***/ }),

/***/ "499e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/listToStyles.js
/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}

// CONCATENATED MODULE: ./node_modules/vue-style-loader/lib/addStylesClient.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return addStylesClient; });
/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/



var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

function addStylesClient (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "4eac":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionButton_vue_vue_type_style_index_0_id_c9d06050_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("a747");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionButton_vue_vue_type_style_index_0_id_c9d06050_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionButton_vue_vue_type_style_index_0_id_c9d06050_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ActionButton_vue_vue_type_style_index_0_id_c9d06050_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "4fc8":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("bbb1");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("a59f9a8e", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "4fdb":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_id_bbbc04c8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cca6");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_id_bbbc04c8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_id_bbbc04c8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_id_bbbc04c8_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "52a7":
/***/ (function(module, exports) {

exports.f = {}.propertyIsEnumerable;


/***/ }),

/***/ "5537":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("8378");
var global = __webpack_require__("7726");
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});

(module.exports = function (key, value) {
  return store[key] || (store[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: core.version,
  mode: __webpack_require__("2d00") ? 'pure' : 'global',
  copyright: '© 2018 Denis Pushkarev (zloirock.ru)'
});


/***/ }),

/***/ "55a9":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("aa4a");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "5ca1":
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__("7726");
var core = __webpack_require__("8378");
var hide = __webpack_require__("32e9");
var redefine = __webpack_require__("2aba");
var ctx = __webpack_require__("9b43");
var PROTOTYPE = 'prototype';

var $export = function (type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] || (global[name] = {}) : (global[name] || {})[PROTOTYPE];
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
  var key, own, out, exp;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    // export native or passed
    out = (own ? target : source)[key];
    // bind timers to global for call from export context
    exp = IS_BIND && own ? ctx(out, global) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // extend global
    if (target) redefine(target, key, out, type & $export.U);
    // export
    if (exports[key] != out) hide(exports, key, exp);
    if (IS_PROTO && expProto[key] != out) expProto[key] = out;
  }
};
global.core = core;
// type bitmap
$export.F = 1;   // forced
$export.G = 2;   // global
$export.S = 4;   // static
$export.P = 8;   // proto
$export.B = 16;  // bind
$export.W = 32;  // wrap
$export.U = 64;  // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;


/***/ }),

/***/ "5cd9":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("2d75");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("52cb8b69", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "5d29":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("3e71");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("98aff794", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "5dbc":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
var setPrototypeOf = __webpack_require__("8b97").set;
module.exports = function (that, target, C) {
  var S = target.constructor;
  var P;
  if (S !== C && typeof S == 'function' && (P = S.prototype) !== C.prototype && isObject(P) && setPrototypeOf) {
    setPrototypeOf(that, P);
  } return that;
};


/***/ }),

/***/ "6050":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("2e1b");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("9ea09b46", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "613b":
/***/ (function(module, exports, __webpack_require__) {

var shared = __webpack_require__("5537")('keys');
var uid = __webpack_require__("ca5a");
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};


/***/ }),

/***/ "626a":
/***/ (function(module, exports, __webpack_require__) {

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__("2d95");
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};


/***/ }),

/***/ "6428":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-bbbc04c8]{box-shadow:none;box-sizing:border-box;color:#333;font-size:14px;font-weight:400;margin:0;outline:0;padding:0;position:relative;user-select:none\n}\n.root:active .caretDown[data-v-bbbc04c8],.root:active .title[data-v-bbbc04c8],.root:focus .caretDown[data-v-bbbc04c8],.root:focus .title[data-v-bbbc04c8],.root:hover .caretDown[data-v-bbbc04c8],.root:hover .title[data-v-bbbc04c8]{color:#212121\n}\n.root:active .titleIsPlaceHolder[data-v-bbbc04c8],.root:focus .titleIsPlaceHolder[data-v-bbbc04c8],.root:hover .titleIsPlaceHolder[data-v-bbbc04c8]{color:#666\n}\n.root:hover .title[data-v-bbbc04c8]{border-color:#212121\n}\n@media screen and (-ms-high-contrast:active){\n.root:hover .title[data-v-bbbc04c8]{border-color:Highlight\n}\n}\n.root:hover .titleIsError[data-v-bbbc04c8]{border-color:#a80000\n}\n.root:active .title[data-v-bbbc04c8]{border-color:#005a9e\n}\n@media screen and (-ms-high-contrast:active){\n.root:active .title[data-v-bbbc04c8]{border-color:Highlight\n}\n}\n.root:active .titleIsError[data-v-bbbc04c8]{border-color:#a80000\n}\n.root:focus .title[data-v-bbbc04c8]{border-color:#0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.root:focus .title[data-v-bbbc04c8]{background-color:Highlight;border-color:Highlight;color:HighlightText\n}\n}\n@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:black-on-white){\n.root:focus .title[data-v-bbbc04c8]{-ms-high-contrast-adjust:none\n}\n}\n@media screen and (-ms-high-contrast:active){\n.root:focus .caretDown[data-v-bbbc04c8]{color:HighlightText\n}\n}\n@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:black-on-white){\n.root:focus .caretDown[data-v-bbbc04c8]{-ms-high-contrast-adjust:none\n}\n}\n.root .titleIsError[data-v-bbbc04c8],.root:focus .titleIsError[data-v-bbbc04c8]{border-color:#a80000\n}\n.root[data-v-bbbc04c8] .ms-Label{display:inline-block;margin-bottom:8px\n}\n.root.rootIsDisabled .title[data-v-bbbc04c8]{background-color:#f4f4f4;border:none;color:#a6a6a6;cursor:default\n}\n@media screen and (-ms-high-contrast:active){\n.root.rootIsDisabled .title[data-v-bbbc04c8]{border:1px solid GrayText;color:GrayText\n}\n}\n.root.rootIsDisabled .title.titleIsError[data-v-bbbc04c8]{border-color:#a80000\n}\n.root.rootIsDisabled .caretDown[data-v-bbbc04c8]{color:#a6a6a6\n}\n@media screen and (-ms-high-contrast:active){\n.root.rootIsDisabled .caretDown[data-v-bbbc04c8]{color:GrayText\n}\n}\n.listWrapper[data-v-bbbc04c8]:focus{outline:0\n}\n.caretDownWrapper[data-v-bbbc04c8]{height:32px;line-height:30px;position:absolute;top:1px\n}\n[dir=ltr] .caretDownWrapper[data-v-bbbc04c8]{right:12px\n}\n[dir=rtl] .caretDownWrapper[data-v-bbbc04c8]{left:12px\n}\n.caretDown[data-v-bbbc04c8]{color:#666;font-size:12px;pointer-events:none\n}\n.title[data-v-bbbc04c8]{background:#fff;border:1px solid #a6a6a6;box-shadow:none;box-sizing:border-box;cursor:pointer;display:block;height:32px;line-height:30px;margin:0;overflow:hidden;padding:0 32px 0 12px;position:relative;text-overflow:ellipsis;white-space:nowrap\n}\nhtml[dir=rtl] .title[data-v-bbbc04c8]{padding:0 12px 0 32px\n}\n.title.titleIsPlaceHolder[data-v-bbbc04c8]{color:#666\n}\n[dir=ltr] .panel[data-v-bbbc04c8] .ms-Panel-main{box-shadow:-30px 0 30px -30px rgba(0,0,0,.2)\n}\n[dir=rtl] .panel[data-v-bbbc04c8] .ms-Panel-main{box-shadow:30px 0 30px -30px rgba(0,0,0,.2)\n}\n.panel[data-v-bbbc04c8] .ms-Panel-contentInner{padding:0 0 20px\n}\n.callout.callout[data-v-bbbc04c8]{border:1px solid #eaeaea;box-shadow:0 0 2px 0 rgba(0,0,0,.2)\n}\n.errorMessage[data-v-bbbc04c8]{color:#a80000;font-size:12px;font-weight:400;padding-top:5px\n}\n.item[data-v-bbbc04c8],.items[data-v-bbbc04c8]{display:block\n}\n.item[data-v-bbbc04c8]{background:transparent;border:1px solid transparent;box-sizing:border-box;cursor:pointer;height:auto!important;line-height:20px;min-height:32px;overflow-wrap:break-word;padding:4px 16px!important;position:relative;text-align:left;width:100%;word-wrap:break-word\n}\n@media screen and (-ms-high-contrast:active){\n.item[data-v-bbbc04c8]{border-color:Window\n}\n}\n.item[data-v-bbbc04c8]:hover{color:inherit\n}\n.item[data-v-bbbc04c8]:focus{background-color:#f4f4f4\n}\n@media screen and (-ms-high-contrast:active){\n.item[data-v-bbbc04c8]:focus{background-color:Highlight;border-color:Highlight;color:HighlightText\n}\n}\n@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:black-on-white){\n.item[data-v-bbbc04c8]:focus{-ms-high-contrast-adjust:none\n}\n}\n.item[data-v-bbbc04c8]:active{background-color:#f4f4f4;color:#000\n}\n.item.itemIsDisabled[data-v-bbbc04c8]{color:#a6a6a6;cursor:default\n}\n.item[data-v-bbbc04c8] .ms-Button-flexContainer{justify-content:flex-start\n}\n.item.itemIsSelected[data-v-bbbc04c8]{background-color:#dadada;color:#000\n}\n@media screen and (-ms-high-contrast:active){\n.item.itemIsSelected[data-v-bbbc04c8]{background-color:Highlight;border-color:Highlight;color:HighlightText\n}\n}\n@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:black-on-white){\n.item.itemIsSelected[data-v-bbbc04c8]{-ms-high-contrast-adjust:none\n}\n}\n.item.itemIsSelected.itemIsDisabled[data-v-bbbc04c8]{background-color:transparent;color:#a6a6a6\n}\n.header[data-v-bbbc04c8]{background:none;background-color:transparent;border:none;color:#0078d4;cursor:default;font-size:14px;font-weight:400;font-weight:600;height:32px;line-height:32px;padding:0 16px;user-select:none\n}\nhtml[dir=ltr] .header[data-v-bbbc04c8]{text-align:left\n}\nhtml[dir=rtl] .header[data-v-bbbc04c8]{text-align:right\n}\n.divider[data-v-bbbc04c8]{background-color:#c8c8c8;height:1px\n}\n.optionText[data-v-bbbc04c8]{margin:1px;max-width:100%;min-width:0;overflow:hidden;overflow-wrap:break-word;text-overflow:ellipsis;white-space:nowrap;word-wrap:break-word\n}", ""]);

// exports


/***/ }),

/***/ "6436":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n@keyframes spinAnimation-data-v-40312040{\n0%{transform:rotate(0deg)\n}\nto{transform:rotate(1turn)\n}\n}\n.circle[data-v-40312040]{animation-duration:1.3s;animation-iteration-count:infinite;animation-name:spinAnimation-data-v-40312040;animation-timing-function:cubic-bezier(.53,.21,.29,.67);border:1.5px solid #c7e0f4;border-image:initial;border-radius:50%;border-top-color:#0078d4;box-sizing:border-box;margin:auto\n}\n.label[data-v-40312040]{color:#0078d4;margin-top:10px;text-align:center\n}", ""]);

// exports


/***/ }),

/***/ "67fc":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Persona_vue_vue_type_style_index_0_id_79af599f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("d0cd");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Persona_vue_vue_type_style_index_0_id_79af599f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Persona_vue_vue_type_style_index_0_id_79af599f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Persona_vue_vue_type_style_index_0_id_79af599f_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "6821":
/***/ (function(module, exports, __webpack_require__) {

// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__("626a");
var defined = __webpack_require__("be13");
module.exports = function (it) {
  return IObject(defined(it));
};


/***/ }),

/***/ "68fd":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("c8c3");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("05c2255e", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "69a8":
/***/ (function(module, exports) {

var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};


/***/ }),

/***/ "6a99":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__("d3f4");
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};


/***/ }),

/***/ "7028":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("7259");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("5ee09f24", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "7259":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.primarySet[data-v-073452f4]{flex-grow:1\n}\n.primarySet[data-v-073452f4],.secondarySet[data-v-073452f4]{align-items:stretch;display:flex;flex-wrap:nowrap;position:relative\n}\n.item[data-v-073452f4],.secondarySet[data-v-073452f4]{flex-shrink:0\n}\n.item[data-v-073452f4]{display:inherit\n}\n.root[data-v-073452f4]{background-color:#f4f4f4;display:flex;height:40px;padding:0 16px\n}\n.root .ms-Button[data-v-073452f4]:hover{background-color:#eaeaea\n}\n.root .ms-Button[data-v-073452f4],.root .ms-Button[data-v-073452f4]:hover{color:#333\n}", ""]);

// exports


/***/ }),

/***/ "7726":
/***/ (function(module, exports) {

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math
  ? window : typeof self != 'undefined' && self.Math == Math ? self
  // eslint-disable-next-line no-new-func
  : Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef


/***/ }),

/***/ "7796":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\nlabel.root[data-v-7c809f01]{box-shadow:none;box-sizing:border-box;color:#333;display:block;margin:0;overflow-wrap:break-word;padding:5px 0;word-wrap:break-word\n}\nlabel.root[data-v-7c809f01]:disabled,label.root[disabled][data-v-7c809f01]{color:#c8c8c8\n}\nlabel.root[data-v-7c809f01]:required:after,label.root[required][data-v-7c809f01]:after{color:#a80000;content:\" *\";padding-right:12px\n}", ""]);

// exports


/***/ }),

/***/ "77f1":
/***/ (function(module, exports, __webpack_require__) {

var toInteger = __webpack_require__("4588");
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};


/***/ }),

/***/ "79a3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButton_vue_vue_type_style_index_0_id_5565ffd4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("5d29");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButton_vue_vue_type_style_index_0_id_5565ffd4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButton_vue_vue_type_style_index_0_id_5565ffd4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BaseButton_vue_vue_type_style_index_0_id_5565ffd4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "79e5":
/***/ (function(module, exports) {

module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};


/***/ }),

/***/ "7a8f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Link_vue_vue_type_style_index_0_id_04588638_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("5cd9");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Link_vue_vue_type_style_index_0_id_04588638_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Link_vue_vue_type_style_index_0_id_04588638_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Link_vue_vue_type_style_index_0_id_04588638_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "7acd":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.ms-OverflowSet{display:flex;flex-wrap:nowrap;position:relative\n}\n.rootVertical{flex-direction:column\n}\n.ms-OverflowSet-item,.ms-OverflowSet-overflowButton{display:inherit;flex-shrink:0\n}\n.ms-OverflowSet-overflowButton button.overflowButton{align-self:stretch;height:auto;min-width:0;padding:0 4px\n}\nhtml[dir=ltr] .overflowLinks{margin-right:10px\n}\nhtml[dir=rtl] .overflowLinks{margin-left:10px\n}", ""]);

// exports


/***/ }),

/***/ "7f7f":
/***/ (function(module, exports, __webpack_require__) {

var dP = __webpack_require__("86cc").f;
var FProto = Function.prototype;
var nameRE = /^\s*function ([^ (]*)/;
var NAME = 'name';

// 19.2.4.2 name
NAME in FProto || __webpack_require__("9e1e") && dP(FProto, NAME, {
  configurable: true,
  get: function () {
    try {
      return ('' + this).match(nameRE)[1];
    } catch (e) {
      return '';
    }
  }
});


/***/ }),

/***/ "8378":
/***/ (function(module, exports) {

var core = module.exports = { version: '2.5.7' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef


/***/ }),

/***/ "86cc":
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__("cb7c");
var IE8_DOM_DEFINE = __webpack_require__("c69a");
var toPrimitive = __webpack_require__("6a99");
var dP = Object.defineProperty;

exports.f = __webpack_require__("9e1e") ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};


/***/ }),

/***/ "8b97":
/***/ (function(module, exports, __webpack_require__) {

// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__("d3f4");
var anObject = __webpack_require__("cb7c");
var check = function (O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
    function (test, buggy, set) {
      try {
        set = __webpack_require__("9b43")(Function.call, __webpack_require__("11e9").f(Object.prototype, '__proto__').set, 2);
        set(test, []);
        buggy = !(test instanceof Array);
      } catch (e) { buggy = true; }
      return function setPrototypeOf(O, proto) {
        check(O, proto);
        if (buggy) O.__proto__ = proto;
        else set(O, proto);
        return O;
      };
    }({}, false) : undefined),
  check: check
};


/***/ }),

/***/ "8c7c":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root.is-active .pill[data-v-986803ca]{background:#0078d4\n}\n.root.is-active .thumb[data-v-986803ca]{background:#fff;float:right\n}\n.root:not(.is-active) .pill[data-v-986803ca]{background:#fff;border-color:#666\n}\n.root:not(.is-active) .thumb[data-v-986803ca]{background:#212121;float:left\n}\n.root.is-disabled .pill[data-v-986803ca]{background:#c8c8c8;cursor:default\n}\n.root.is-disabled.is-active .ms-Label[data-v-986803ca],.root.is-disabled.is-active .text[data-v-986803ca],.root.is-disabled .ms-Label[data-v-986803ca],.root.is-disabled .text[data-v-986803ca]{color:#a6a6a6\n}\n.root.is-disabled:not(.is-active) .pill[data-v-986803ca]{background:#fff;border-color:#a6a6a6\n}\n.root.is-disabled:not(.is-active) .thumb[data-v-986803ca]{background:#a6a6a6\n}\n.root .label[data-v-986803ca]{display:block\n}\n.root .container[data-v-986803ca]{display:inline-block\n}\n.root .pill[data-v-986803ca]{align-items:center;border:1px solid transparent;border-radius:1em;cursor:pointer;font-size:20px;height:1em;outline:transparent;padding:0 .2em;position:relative;width:2.2em\n}\n.root .pill[data-v-986803ca],.root .thumb[data-v-986803ca]{box-sizing:border-box;transition:all .1s ease\n}\n.root .thumb[data-v-986803ca]{background-color:#fff;border:.28em solid transparent;border-radius:.5em;height:.5em;width:.5em\n}\n.root .pill[data-v-986803ca],.root .text[data-v-986803ca]{display:inline-block;vertical-align:middle\n}\n.root .text[data-v-986803ca]{margin:0 10px\n}\n.root .root[data-v-986803ca],.root .text[data-v-986803ca]{user-select:none\n}", ""]);

// exports


/***/ }),

/***/ "8e34":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "9093":
/***/ (function(module, exports, __webpack_require__) {

// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__("ce10");
var hiddenKeys = __webpack_require__("e11e").concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};


/***/ }),

/***/ "92fa":
/***/ (function(module, exports) {

var nestRE = /^(attrs|props|on|nativeOn|class|style|hook)$/

module.exports = function mergeJSXProps (objs) {
  return objs.reduce(function (a, b) {
    var aa, bb, key, nestedKey, temp
    for (key in b) {
      aa = a[key]
      bb = b[key]
      if (aa && nestRE.test(key)) {
        // normalize class
        if (key === 'class') {
          if (typeof aa === 'string') {
            temp = aa
            a[key] = aa = {}
            aa[temp] = true
          }
          if (typeof bb === 'string') {
            temp = bb
            b[key] = bb = {}
            bb[temp] = true
          }
        }
        if (key === 'on' || key === 'nativeOn' || key === 'hook') {
          // merge functions
          for (nestedKey in bb) {
            aa[nestedKey] = mergeFn(aa[nestedKey], bb[nestedKey])
          }
        } else if (Array.isArray(aa)) {
          a[key] = aa.concat(bb)
        } else if (Array.isArray(bb)) {
          a[key] = [aa].concat(bb)
        } else {
          for (nestedKey in bb) {
            aa[nestedKey] = bb[nestedKey]
          }
        }
      } else {
        a[key] = b[key]
      }
    }
    return a
  }, {})
}

function mergeFn (a, b) {
  return function () {
    a && a.apply(this, arguments)
    b && b.apply(this, arguments)
  }
}


/***/ }),

/***/ "948b":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("28cc");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("0951c7b1", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "9a6e":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("8c7c");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("2bce8a87", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "9b43":
/***/ (function(module, exports, __webpack_require__) {

// optional / simple context binding
var aFunction = __webpack_require__("d8e8");
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};


/***/ }),

/***/ "9d0e":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProgressIndicator_vue_vue_type_style_index_0_id_8180d784_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("e3fc");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProgressIndicator_vue_vue_type_style_index_0_id_8180d784_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProgressIndicator_vue_vue_type_style_index_0_id_8180d784_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProgressIndicator_vue_vue_type_style_index_0_id_8180d784_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "9def":
/***/ (function(module, exports, __webpack_require__) {

// 7.1.15 ToLength
var toInteger = __webpack_require__("4588");
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};


/***/ }),

/***/ "9e1e":
/***/ (function(module, exports, __webpack_require__) {

// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__("79e5")(function () {
  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "a387":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_id_7830d5ff_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("38c2");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_id_7830d5ff_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_id_7830d5ff_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Slider_vue_vue_type_style_index_0_id_7830d5ff_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "a53f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CompoundButton_vue_vue_type_style_index_0_id_3ddee351_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6050");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CompoundButton_vue_vue_type_style_index_0_id_3ddee351_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CompoundButton_vue_vue_type_style_index_0_id_3ddee351_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CompoundButton_vue_vue_type_style_index_0_id_3ddee351_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "a747":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("b99e");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("16fbcc6d", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "aa4a":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("8e34");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("38191d2f", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "aa77":
/***/ (function(module, exports, __webpack_require__) {

var $export = __webpack_require__("5ca1");
var defined = __webpack_require__("be13");
var fails = __webpack_require__("79e5");
var spaces = __webpack_require__("fdef");
var space = '[' + spaces + ']';
var non = '\u200b\u0085';
var ltrim = RegExp('^' + space + space + '*');
var rtrim = RegExp(space + space + '*$');

var exporter = function (KEY, exec, ALIAS) {
  var exp = {};
  var FORCE = fails(function () {
    return !!spaces[KEY]() || non[KEY]() != non;
  });
  var fn = exp[KEY] = FORCE ? exec(trim) : spaces[KEY];
  if (ALIAS) exp[ALIAS] = fn;
  $export($export.P + $export.F * FORCE, 'String', exp);
};

// 1 -> String#trimLeft
// 2 -> String#trimRight
// 3 -> String#trim
var trim = exporter.trim = function (string, TYPE) {
  string = String(defined(string));
  if (TYPE & 1) string = string.replace(ltrim, '');
  if (TYPE & 2) string = string.replace(rtrim, '');
  return string;
};

module.exports = exporter;


/***/ }),

/***/ "aae3":
/***/ (function(module, exports, __webpack_require__) {

// 7.2.8 IsRegExp(argument)
var isObject = __webpack_require__("d3f4");
var cof = __webpack_require__("2d95");
var MATCH = __webpack_require__("2b4c")('match');
module.exports = function (it) {
  var isRegExp;
  return isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : cof(it) == 'RegExp');
};


/***/ }),

/***/ "b1e9":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("db0d");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("0c5b8f5a", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "b90f":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DefaultButton_vue_vue_type_style_index_0_id_54167381_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("1c6b");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DefaultButton_vue_vue_type_style_index_0_id_54167381_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DefaultButton_vue_vue_type_style_index_0_id_54167381_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DefaultButton_vue_vue_type_style_index_0_id_54167381_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "b99e":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-c9d06050]{background-color:transparent;font-weight:400\n}\n.root .icon[data-v-c9d06050]{color:#106ebe\n}\n.root[data-v-c9d06050]:hover{background-color:transparent\n}\n.root[data-v-c9d06050]:hover,.root:hover .icon[data-v-c9d06050]{color:#0078d4\n}\n.root .label[data-v-c9d06050]{font-weight:400\n}", ""]);

// exports


/***/ }),

/***/ "bafc":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_1256c1e4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("68fd");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_1256c1e4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_1256c1e4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchBox_vue_vue_type_style_index_0_id_1256c1e4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "bbb1":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-8c974350]{margin:23px 0 1px\n}\n.list[data-v-8c974350]{align-items:stretch;white-space:nowrap\n}\n.list[data-v-8c974350],.listItem[data-v-8c974350]{display:flex;margin:0;padding:0\n}\n.listItem[data-v-8c974350]{align-items:center;list-style-type:none;position:relative\n}\n.itemLink[data-v-8c974350]{-webkit-font-smoothing:antialiased;background:none transparent;border:initial none;border-color:initial;border-image:initial;color:#333;cursor:pointer;display:inline;font-family:Segoe UI,\"Segoe UI Web (West European)\",-apple-system,BlinkMacSystemFont,Roboto,Helvetica Neue,sans-serif;font-size:21px;font-weight:100;line-height:31px;margin:0;max-width:160px;outline:transparent;overflow:hidden;padding:0 8px;position:relative;text-align:left;text-decoration:none;text-overflow:ellipsis;user-select:text;white-space:nowrap\n}\n.itemLink[data-v-8c974350]:hover{background-color:#f4f4f4;color:initial;cursor:pointer\n}\n.chevron[data-v-8c974350]{-webkit-font-smoothing:antialiased;color:#666;display:inline-block;font-family:FabricMDL2Icons;font-size:12px;font-style:normal;font-weight:400;speak:none\n}\n.overflow[data-v-8c974350]{align-items:center;display:flex;position:relative\n}\n.overflowButton[data-v-8c974350]{background-color:transparent;border:1px solid transparent;border-image:initial;border-radius:0;box-sizing:border-box;cursor:pointer;display:inline-block;font-size:16px;font-weight:400;height:100%;outline:transparent;overflow:hidden;padding:0 4px;position:relative;text-align:center;text-decoration:none;text-overflow:ellipsis;user-select:none;vertical-align:top;white-space:nowrap;width:32px\n}", ""]);

// exports


/***/ }),

/***/ "bc41":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-7830d5ff]{user-select:none\n}\n.titleLabel[data-v-7830d5ff]{padding:0\n}\n.line[data-v-7830d5ff]{display:flex;position:relative\n}\n.line .lineContainer[data-v-7830d5ff]{border-radius:4px;box-sizing:border-box\n}\n.container[data-v-7830d5ff]{align-items:center;display:flex;flex-wrap:nowrap\n}\n.valueLabel[data-v-7830d5ff]{flex-shrink:1;line-height:1;width:30px\n}\n.slideBox[data-v-7830d5ff]{background:transparent;border:none;flex-grow:1;line-height:28px\n}\n.slideBox[data-v-7830d5ff]::-moz-focus-inner{border:0\n}\n.slideBox[data-v-7830d5ff]{outline:transparent;position:relative\n}\n.ms-Fabric.is-focusVisible .slideBox[data-v-7830d5ff]:focus:after{border:1px solid #000;bottom:0;content:\"\";left:0;pointer-events:none;position:absolute;right:0;top:0\n}\n.slideBox .thumb[data-v-7830d5ff]{background:#fff;border:2px solid #666;border-radius:10px;box-sizing:border-box;display:block;height:16px;position:absolute;width:16px\n}\n.rootIsHorizontal .line[data-v-7830d5ff]{width:100%\n}\n.rootIsHorizontal .line .lineContainer[data-v-7830d5ff]{height:4px\n}\n.rootIsHorizontal .slideBox[data-v-7830d5ff]{height:28px;padding:0 8px\n}\n.rootIsHorizontal .slideBox .thumb[data-v-7830d5ff]{top:-6px\n}\nhtml[dir=ltr] .rootIsHorizontal .slideBox .thumb[data-v-7830d5ff]{transform:translateX(-50%)\n}\nhtml[dir=rtl] .rootIsHorizontal .slideBox .thumb[data-v-7830d5ff]{transform:translateX(50%)\n}\n.rootIsHorizontal .valueLabel[data-v-7830d5ff]{margin:0 8px;white-space:nowrap;width:40px\n}\n.rootIsVertical[data-v-7830d5ff]{margin-right:8px\n}\n.rootIsVertical .line[data-v-7830d5ff]{flex-direction:column-reverse;height:100%;margin:0 auto;width:4px\n}\n.rootIsVertical .line .lineContainer[data-v-7830d5ff]{height:100%;width:4px\n}\n.rootIsVertical .slideBox[data-v-7830d5ff]{height:100%;padding:8px 0;width:28px\n}\n.rootIsVertical .slideBox .thumb[data-v-7830d5ff]{left:-6px;margin:0 auto;transform:translateY(8px)\n}\n.rootIsVertical .container[data-v-7830d5ff]{flex-direction:column;height:100%;margin:8px 0;text-align:center\n}\n.rootIsVertical .valueLabel[data-v-7830d5ff]{margin:0 auto;white-space:nowrap;width:40px\n}\n.activeSection[data-v-7830d5ff]{background:#666\n}\n@media screen and (-ms-high-contrast:active){\n.activeSection[data-v-7830d5ff]{background-color:WindowText\n}\n}\n.inactiveSection[data-v-7830d5ff]{background:#c8c8c8\n}\n@media screen and (-ms-high-contrast:active){\n.inactiveSection[data-v-7830d5ff]{border:1px solid WindowText\n}\n}\n.showTransitions .thumb[data-v-7830d5ff]{transition:left .367s cubic-bezier(.1,.9,.2,1)\n}\n.showTransitions .activeSection[data-v-7830d5ff],.showTransitions .inactiveSection[data-v-7830d5ff]{transition:width .367s cubic-bezier(.1,.9,.2,1)\n}\n.rootIsEnabled .slideBox:active .thumb[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .thumb[data-v-7830d5ff]{border:2px solid #0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.rootIsEnabled .slideBox:active .thumb[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .thumb[data-v-7830d5ff]{border-color:Highlight\n}\n}\n.rootIsEnabled .slideBox:active .activeSection[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .activeSection[data-v-7830d5ff]{background-color:#0078d4\n}\n@media screen and (-ms-high-contrast:active){\n.rootIsEnabled .slideBox:active .activeSection[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .activeSection[data-v-7830d5ff]{background-color:Highlight\n}\n}\n.rootIsEnabled .slideBox:active .inactiveSection[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .inactiveSection[data-v-7830d5ff]{background-color:#c7e0f4\n}\n@media screen and (-ms-high-contrast:active){\n.rootIsEnabled .slideBox:active .inactiveSection[data-v-7830d5ff],.rootIsEnabled .slideBox:hover .inactiveSection[data-v-7830d5ff]{border-color:Highlight\n}\n}\n.rootIsEnabled .slideBox:active .thumb[data-v-7830d5ff]{border:2px solid #106ebe\n}\n.rootIsEnabled .slideBox:active .activeSection[data-v-7830d5ff]{background-color:#106ebe\n}\n.rootIsDisabled .thumb[data-v-7830d5ff]{border-color:#c8c8c8\n}\n@media screen and (-ms-high-contrast:active){\n.rootIsDisabled .thumb[data-v-7830d5ff]{border-color:GrayText\n}\n}\n@media screen and (-ms-high-contrast:active){\n.rootIsDisabled .activeSection[data-v-7830d5ff],.rootIsDisabled .inactiveSection[data-v-7830d5ff]{background-color:GrayText;border-color:GrayText\n}\n}\n.rootIsDisabled .activeSection[data-v-7830d5ff]{background:#c8c8c8\n}\n.rootIsDisabled .inactiveSection[data-v-7830d5ff]{background:#eaeaea\n}", ""]);

// exports


/***/ }),

/***/ "be13":
/***/ (function(module, exports) {

// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};


/***/ }),

/***/ "c259":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IconButton_vue_vue_type_style_index_0_id_5897742a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("b1e9");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IconButton_vue_vue_type_style_index_0_id_5897742a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IconButton_vue_vue_type_style_index_0_id_5897742a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_IconButton_vue_vue_type_style_index_0_id_5897742a_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "c31a":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("286a");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("b75b9e3e", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "c366":
/***/ (function(module, exports, __webpack_require__) {

// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__("6821");
var toLength = __webpack_require__("9def");
var toAbsoluteIndex = __webpack_require__("77f1");
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
      if (O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};


/***/ }),

/***/ "c5b1":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("7acd");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("5b06df44", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "c5f6":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var global = __webpack_require__("7726");
var has = __webpack_require__("69a8");
var cof = __webpack_require__("2d95");
var inheritIfRequired = __webpack_require__("5dbc");
var toPrimitive = __webpack_require__("6a99");
var fails = __webpack_require__("79e5");
var gOPN = __webpack_require__("9093").f;
var gOPD = __webpack_require__("11e9").f;
var dP = __webpack_require__("86cc").f;
var $trim = __webpack_require__("aa77").trim;
var NUMBER = 'Number';
var $Number = global[NUMBER];
var Base = $Number;
var proto = $Number.prototype;
// Opera ~12 has broken Object#toString
var BROKEN_COF = cof(__webpack_require__("2aeb")(proto)) == NUMBER;
var TRIM = 'trim' in String.prototype;

// 7.1.3 ToNumber(argument)
var toNumber = function (argument) {
  var it = toPrimitive(argument, false);
  if (typeof it == 'string' && it.length > 2) {
    it = TRIM ? it.trim() : $trim(it, 3);
    var first = it.charCodeAt(0);
    var third, radix, maxCode;
    if (first === 43 || first === 45) {
      third = it.charCodeAt(2);
      if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
    } else if (first === 48) {
      switch (it.charCodeAt(1)) {
        case 66: case 98: radix = 2; maxCode = 49; break; // fast equal /^0b[01]+$/i
        case 79: case 111: radix = 8; maxCode = 55; break; // fast equal /^0o[0-7]+$/i
        default: return +it;
      }
      for (var digits = it.slice(2), i = 0, l = digits.length, code; i < l; i++) {
        code = digits.charCodeAt(i);
        // parseInt parses a string to a first unavailable symbol
        // but ToNumber should return NaN if a string contains unavailable symbols
        if (code < 48 || code > maxCode) return NaN;
      } return parseInt(digits, radix);
    }
  } return +it;
};

if (!$Number(' 0o1') || !$Number('0b1') || $Number('+0x1')) {
  $Number = function Number(value) {
    var it = arguments.length < 1 ? 0 : value;
    var that = this;
    return that instanceof $Number
      // check on 1..constructor(foo) case
      && (BROKEN_COF ? fails(function () { proto.valueOf.call(that); }) : cof(that) != NUMBER)
        ? inheritIfRequired(new Base(toNumber(it)), that, $Number) : toNumber(it);
  };
  for (var keys = __webpack_require__("9e1e") ? gOPN(Base) : (
    // ES3:
    'MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,' +
    // ES6 (in case, if modules with ES6 Number statics required before):
    'EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,' +
    'MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger'
  ).split(','), j = 0, key; keys.length > j; j++) {
    if (has(Base, key = keys[j]) && !has($Number, key)) {
      dP($Number, key, gOPD(Base, key));
    }
  }
  $Number.prototype = proto;
  proto.constructor = $Number;
  __webpack_require__("2aba")(global, NUMBER, $Number);
}


/***/ }),

/***/ "c629":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContextualMenu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("df53");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContextualMenu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContextualMenu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_6_oneOf_1_0_node_modules_css_loader_index_js_ref_6_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_6_oneOf_1_2_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ContextualMenu_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "c69a":
/***/ (function(module, exports, __webpack_require__) {

module.exports = !__webpack_require__("9e1e") && !__webpack_require__("79e5")(function () {
  return Object.defineProperty(__webpack_require__("230e")('div'), 'a', { get: function () { return 7; } }).a != 7;
});


/***/ }),

/***/ "c8c3":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root.is-active>.iconContainer[data-v-1256c1e4]{width:4px\n}\n.root.is-active>.iconContainer>i[data-v-1256c1e4]{opacity:0\n}\n.root.is-active>.field[data-v-1256c1e4]{width:calc(100% - 36px)\n}\n.root.can-clear:not(.is-active)>.field[data-v-1256c1e4]{width:calc(100% - 64px)\n}\n.root.is-disabled[data-v-1256c1e4]{background-color:#f8f8f8;border-color:#f8f8f8;color:#a6a6a6\n}\n.root.is-disabled[data-v-1256c1e4],.root.is-disabled>*[data-v-1256c1e4]{cursor:default\n}\n.root.is-disabled>.iconContainer[data-v-1256c1e4]{color:#a6a6a6\n}\n.root.is-disabled>.field[data-v-1256c1e4]{background-color:#fff;color:#fff\n}\n.root.is-disabled>.clearButton[data-v-1256c1e4]{background-color:#f8f8f8\n}\n.root.is-underlined.is-disabled[data-v-1256c1e4]{background-color:#fff;border-bottom-color:#f8f8f8\n}\n.ms-SearchBox-container[data-v-1256c1e4]{width:100%\n}\n.root[data-v-1256c1e4]{background-color:#fff;border:1px solid #a6a6a6;color:#333;font-size:0;font-weight:400;padding-left:8px\n}\n.root input[data-v-1256c1e4]::-ms-clear{display:none\n}\n.root.is-underlined[data-v-1256c1e4]{border:0;border-bottom:1px solid #a6a6a6\n}\n.root>.iconContainer[data-v-1256c1e4]{float:left;font-size:16px;transition:width .167s\n}\n.root>.iconContainer>i[data-v-1256c1e4]{opacity:1;transition:opacity .167s 0s\n}\n.root>.field[data-v-1256c1e4]{border:none;color:#333;float:left;font-size:14px;height:30px;margin:0;outline:1px solid transparent;overflow:hidden;padding:0;text-overflow:ellipsis;transition:width .167s 0s;width:calc(100% - 32px)\n}\n.root>.clearButton[data-v-1256c1e4],.root>.iconContainer[data-v-1256c1e4]{color:#0078d4;text-align:center;width:32px\n}\n.root>.clearButton>.ms-Icon[data-v-1256c1e4],.root>.iconContainer>.ms-Icon[data-v-1256c1e4]{line-height:30px\n}\n.root>.clearButton[data-v-1256c1e4]{float:right\n}\n.root>.clearButton[data-v-1256c1e4]:hover{cursor:pointer\n}\n.root>.clearButton>i[data-v-1256c1e4],.root>.clearButton>i[data-v-1256c1e4]:before{font-size:12px\n}", ""]);

// exports


/***/ }),

/***/ "ca5a":
/***/ (function(module, exports) {

var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};


/***/ }),

/***/ "cb7c":
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__("d3f4");
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};


/***/ }),

/***/ "cca6":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("6428");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("3aa0e6c2", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "ce10":
/***/ (function(module, exports, __webpack_require__) {

var has = __webpack_require__("69a8");
var toIObject = __webpack_require__("6821");
var arrayIndexOf = __webpack_require__("c366")(false);
var IE_PROTO = __webpack_require__("613b")('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) if (key != IE_PROTO) has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~arrayIndexOf(result, key) || result.push(key);
  }
  return result;
};


/***/ }),

/***/ "cf90":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("7796");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("58cb6ad5", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "d0cd":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("e82e");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("5a892bb3", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "d3f4":
/***/ (function(module, exports) {

module.exports = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};


/***/ }),

/***/ "d444":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("6436");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("127774c8", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "d8e8":
/***/ (function(module, exports) {

module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};


/***/ }),

/***/ "da78":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-8180d784]{font-weight:400\n}\n.itemName[data-v-8180d784]{color:#333;font-size:14px;line-height:20px;overflow:hidden;padding-top:4px;text-overflow:ellipsis;white-space:nowrap\n}\n.itemDescription[data-v-8180d784]{color:#767676;font-size:11px;line-height:18px\n}\n.itemProgress[data-v-8180d784]{height:2px;overflow:hidden;padding:8px 0;position:relative\n}\n.progressTrack[data-v-8180d784]{background-color:#eaeaea;height:2px;position:absolute;width:100%\n}\n@media screen and (-ms-high-contrast:active){\n.progressTrack[data-v-8180d784]{border-bottom:1px solid WindowText\n}\n}\n.progressBar[data-v-8180d784]{background-color:#0078d4;height:2px;position:absolute;transition:width .3s ease;width:0\n}\n.progressBar.indeterminate[data-v-8180d784]{animation:indeterminateProgress-data-v-8180d784 3s infinite;background:linear-gradient(90deg,transparent 0,#0078d4 50%,transparent);min-width:33%;position:absolute\n}\n@media screen and (-ms-high-contrast:active){\n.progressBar[data-v-8180d784]{background-color:WindowText\n}\n}\nhtml[dir=rtl] .progressBar.indeterminate[data-v-8180d784]{animation:indeterminateProgressRTL-data-v-8180d784 3s infinite\n}\n.smoothTransition[data-v-8180d784]{transition-duration:.15s;transition-property:width;transition-timing-function:linear\n}\n@keyframes indeterminateProgress-data-v-8180d784{\n0%{left:-30%\n}\nto{left:100%\n}\n}\n@keyframes indeterminateProgressRTL-data-v-8180d784{\n0%{right:-30%\n}\nto{right:100%\n}\n}", ""]);

// exports


/***/ }),

/***/ "db0d":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.root[data-v-5897742a]{background-color:transparent;height:32px;padding:0 4px;width:32px\n}", ""]);

// exports


/***/ }),

/***/ "df53":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("4215");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("9ade5e0a", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "e11e":
/***/ (function(module, exports) {

// IE 8- don't enum bug keys
module.exports = (
  'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
).split(',');


/***/ }),

/***/ "e3cb":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.ms-Checkbox.root[data-v-fc1d4600]{background:none;border:initial none;border-color:initial;border-image:initial;cursor:pointer!important;display:block;font-size:14px;line-height:21px;margin:0;outline:none;padding:0;position:relative\n}\n.ms-Checkbox.root.is-checked .checkbox>i[data-v-fc1d4600]{color:#fff;opacity:1\n}\n.ms-Checkbox.root:not(.is-checked) .checkbox>i[data-v-fc1d4600]{opacity:0\n}\n.ms-Checkbox.root.is-checked.is-disabled .checkbox[data-v-fc1d4600]{background-color:#c8c8c8;border-color:#c8c8c8\n}\n.ms-Checkbox.root.is-checked:not(.is-disabled) .checkbox[data-v-fc1d4600]{background-color:#0078d4;border-color:#0078d4\n}\n.ms-Checkbox.root.is-disabled:not(.is-checked) .checkbox[data-v-fc1d4600]{border-color:#c8c8c8\n}\n.ms-Checkbox.root .label[data-v-fc1d4600]{cursor:pointer;display:inline-block;height:22px\n}\n.ms-Checkbox.root .checkbox[data-v-fc1d4600]{border-style:solid;border-width:1px;display:inline-block;height:20px;margin:0 4px;transition-duration:.2s;transition-property:background,border,border-color;transition-timing-function:cubic-bezier(.4,0,.23,1);vertical-align:baseline;width:20px\n}\n.ms-Checkbox.root .checkbox>i[data-v-fc1d4600]{display:inline-block;vertical-align:top\n}\n.ms-Checkbox.root .text[data-v-fc1d4600]{color:#333;display:inline-block;margin:0 4px;vertical-align:baseline\n}", ""]);

// exports


/***/ }),

/***/ "e3fc":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("da78");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__("499e").default
var update = add("1eefa131", content, true, {"sourceMap":false,"shadowMode":false});

/***/ }),

/***/ "e82e":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("2350")(false);
// imports


// module
exports.push([module.i, "\n.ms-Persona[size=\"14\"]>.ms-Persona-details[data-v-79af599f]{width:calc(100% - 87px)\n}\n.ms-Persona[size=\"9\"]>.ms-Persona-coin[data-v-79af599f]{vertical-align:top\n}\n.ms-Persona[size=\"9\"]>.ms-Persona-details[data-v-79af599f]{width:calc(100% - 25px)\n}\n.ms-Persona[data-v-79af599f]{height:auto!important\n}\n.ms-Persona>.ms-Persona-coin[data-v-79af599f]{display:inline-block;font-size:10px;position:relative;vertical-align:middle\n}\n.ms-Persona>.ms-Persona-coin>.ms-Icon[data-v-79af599f]{padding-top:4px\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea[data-v-79af599f]{height:72px;position:relative;text-align:center;width:72px\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea .ms-Persona-presence[data-v-79af599f]{background-color:green;border:2px solid #fff;border-radius:50%;bottom:-2px;height:28px;position:absolute;right:-2px;width:28px\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea>.ms-Persona-initials[data-v-79af599f]{background-color:#1e7145;border-radius:50%;color:#fff;font-size:28px;height:72px;line-height:72px\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea>.ms-Persona-image[data-v-79af599f]{border-radius:50%;overflow:hidden\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea>.ms-Image[data-v-79af599f]{height:72px;width:72px\n}\n.ms-Persona>.ms-Persona-coin>.ms-Persona-imageArea>.ms-Image>.ms-Image-image[data-v-79af599f]{height:auto;position:relative;width:100%\n}\n.ms-Persona>.ms-Persona-details[data-v-79af599f]{display:inline-block;padding-left:10px;vertical-align:middle\n}\n.ms-Persona>.ms-Persona-details .ms-Persona-primaryText[data-v-79af599f]{font-size:16px;padding-bottom:1px\n}", ""]);

// exports


/***/ }),

/***/ "eb1a":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Label_vue_vue_type_style_index_0_id_7c809f01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cf90");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Label_vue_vue_type_style_index_0_id_7c809f01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Label_vue_vue_type_style_index_0_id_7c809f01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Label_vue_vue_type_style_index_0_id_7c809f01_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "fab2":
/***/ (function(module, exports, __webpack_require__) {

var document = __webpack_require__("7726").document;
module.exports = document && document.documentElement;


/***/ }),

/***/ "fae3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
var setPublicPath = __webpack_require__("1eb2");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Breadcrumb/Breadcrumb.vue?vue&type=template&id=8c974350&scoped=true&
var Breadcrumbvue_type_template_id_8c974350_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Breadcrumb root",class:_vm.customClasses},[_c('ol',{staticClass:"ms-Breadcrumb-list list"},_vm._l((_vm.items),function(item,index){return _c('li',{key:index,staticClass:"ms-Breadcrumb-listItem listItem"},[_c('button',{staticClass:"ms-Link ms-Breadcrumb-itemLink itemLink"},[_vm._v("\n        "+_vm._s(item.text)+"\n      ")]),(index < _vm.items.length - 1)?_c('icon',{staticClass:"ms-Breadcrumb-chevron chevron",attrs:{"icon-name":"ChevronRight"}}):_vm._e()],1)}))])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Breadcrumb/Breadcrumb.vue?vue&type=template&id=8c974350&scoped=true&

// EXTERNAL MODULE: ./node_modules/babel-helper-vue-jsx-merge-props/index.js
var babel_helper_vue_jsx_merge_props = __webpack_require__("92fa");
var babel_helper_vue_jsx_merge_props_default = /*#__PURE__*/__webpack_require__.n(babel_helper_vue_jsx_merge_props);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Icon/Icon.vue?vue&type=script&lang=js&

/* harmony default export */ var Iconvue_type_script_lang_js_ = ({
  functional: true,
  props: {
    iconName: {
      type: String,
      default: ''
    }
  },
  render: function render(h, ctx) {
    var data = ctx.data;
    var classes = {
      class: ["ms-Icon--".concat(ctx.props.iconName)]
    };
    return h("i", babel_helper_vue_jsx_merge_props_default()([{
      "class": "ms-Icon"
    }, classes, data, {
      attrs: {
        "aria-hidden": "true"
      }
    }]));
  }
});
// CONCATENATED MODULE: ./src/components/Icon/Icon.vue?vue&type=script&lang=js&
 /* harmony default export */ var Icon_Iconvue_type_script_lang_js_ = (Iconvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Icon/Icon.vue?vue&type=style&index=0&lang=scss&
var Iconvue_type_style_index_0_lang_scss_ = __webpack_require__("55a9");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/Icon/Icon.vue
var Icon_render, Icon_staticRenderFns





/* normalize component */

var component = normalizeComponent(
  Icon_Iconvue_type_script_lang_js_,
  Icon_render,
  Icon_staticRenderFns,
  false,
  null,
  null,
  null
  
)

component.options.__file = "Icon.vue"
/* harmony default export */ var Icon = (component.exports);
// CONCATENATED MODULE: ./src/components/Icon/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Breadcrumb/Breadcrumb.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var Breadcrumbvue_type_script_lang_js_ = ({
  components: {
    Icon: Icon
  },
  props: {
    items: {
      type: Array,
      required: true
    },
    customClasses: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/Breadcrumb/Breadcrumb.vue?vue&type=script&lang=js&
 /* harmony default export */ var Breadcrumb_Breadcrumbvue_type_script_lang_js_ = (Breadcrumbvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Breadcrumb/Breadcrumb.vue?vue&type=style&index=0&id=8c974350&lang=scss&scoped=true&
var Breadcrumbvue_type_style_index_0_id_8c974350_lang_scss_scoped_true_ = __webpack_require__("391d");

// CONCATENATED MODULE: ./src/components/Breadcrumb/Breadcrumb.vue






/* normalize component */

var Breadcrumb_component = normalizeComponent(
  Breadcrumb_Breadcrumbvue_type_script_lang_js_,
  Breadcrumbvue_type_template_id_8c974350_scoped_true_render,
  staticRenderFns,
  false,
  null,
  "8c974350",
  null
  
)

Breadcrumb_component.options.__file = "Breadcrumb.vue"
/* harmony default export */ var Breadcrumb = (Breadcrumb_component.exports);
// CONCATENATED MODULE: ./src/components/Breadcrumb/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/DefaultButton/DefaultButton.vue?vue&type=template&id=54167381&scoped=true&
var DefaultButtonvue_type_template_id_54167381_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('base-button',_vm._b({staticClass:"ms-Button--default"},'base-button',_vm.$props,false),[_c('div',{staticClass:"ms-Button-textContainer textContainer"},[_c('div',{staticClass:"ms-Button-label label"},[_vm._t("default",[_vm._v(_vm._s(_vm.text))])],2)])])}
var DefaultButtonvue_type_template_id_54167381_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Button/DefaultButton/DefaultButton.vue?vue&type=template&id=54167381&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/BaseButton.vue?vue&type=template&id=5565ffd4&scoped=true&
var BaseButtonvue_type_template_id_5565ffd4_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.href ? 'a' : 'button',{tag:"component",staticClass:"ms-Button root",class:_vm.classObj,attrs:{"disabled":_vm.disabled,"type":"button"}},[_c('div',{staticClass:"ms-Button-flexContainer flexContainer"},[_vm._t("default")],2)])}
var BaseButtonvue_type_template_id_5565ffd4_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Button/BaseButton.vue?vue&type=template&id=5565ffd4&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/BaseButton.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var BaseButtonvue_type_script_lang_js_ = ({
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    primary: {
      type: Boolean,
      default: false
    },
    text: {
      type: String,
      default: ''
    },
    href: {
      type: String,
      default: null
    }
  },
  computed: {
    classObj: function classObj() {
      return {
        'ms-Button--primary': this.primary,
        'is-disabled': this.disabled
      };
    }
  }
});
// CONCATENATED MODULE: ./src/components/Button/BaseButton.vue?vue&type=script&lang=js&
 /* harmony default export */ var Button_BaseButtonvue_type_script_lang_js_ = (BaseButtonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Button/BaseButton.vue?vue&type=style&index=0&id=5565ffd4&lang=scss&scoped=true&
var BaseButtonvue_type_style_index_0_id_5565ffd4_lang_scss_scoped_true_ = __webpack_require__("79a3");

// CONCATENATED MODULE: ./src/components/Button/BaseButton.vue






/* normalize component */

var BaseButton_component = normalizeComponent(
  Button_BaseButtonvue_type_script_lang_js_,
  BaseButtonvue_type_template_id_5565ffd4_scoped_true_render,
  BaseButtonvue_type_template_id_5565ffd4_scoped_true_staticRenderFns,
  false,
  null,
  "5565ffd4",
  null
  
)

BaseButton_component.options.__file = "BaseButton.vue"
/* harmony default export */ var BaseButton = (BaseButton_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/DefaultButton/DefaultButton.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var DefaultButtonvue_type_script_lang_js_ = ({
  components: {
    BaseButton: BaseButton
  },
  extends: BaseButton
});
// CONCATENATED MODULE: ./src/components/Button/DefaultButton/DefaultButton.vue?vue&type=script&lang=js&
 /* harmony default export */ var DefaultButton_DefaultButtonvue_type_script_lang_js_ = (DefaultButtonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Button/DefaultButton/DefaultButton.vue?vue&type=style&index=0&id=54167381&lang=scss&scoped=true&
var DefaultButtonvue_type_style_index_0_id_54167381_lang_scss_scoped_true_ = __webpack_require__("b90f");

// CONCATENATED MODULE: ./src/components/Button/DefaultButton/DefaultButton.vue






/* normalize component */

var DefaultButton_component = normalizeComponent(
  DefaultButton_DefaultButtonvue_type_script_lang_js_,
  DefaultButtonvue_type_template_id_54167381_scoped_true_render,
  DefaultButtonvue_type_template_id_54167381_scoped_true_staticRenderFns,
  false,
  null,
  "54167381",
  null
  
)

DefaultButton_component.options.__file = "DefaultButton.vue"
/* harmony default export */ var DefaultButton = (DefaultButton_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/ActionButton/ActionButton.vue?vue&type=template&id=c9d06050&scoped=true&
var ActionButtonvue_type_template_id_c9d06050_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('base-button',_vm._b({staticClass:"ms-Button--action ms-Button-command"},'base-button',_vm.$props,false),[(_vm.iconName)?_c('icon',{staticClass:"icon",attrs:{"icon-name":_vm.iconName}}):_vm._e(),_c('div',{staticClass:"ms-Button-textContainer textContainer"},[_c('div',{staticClass:"ms-Button-label label"},[_vm._t("default",[_vm._v(_vm._s(_vm.text))])],2)])],1)}
var ActionButtonvue_type_template_id_c9d06050_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Button/ActionButton/ActionButton.vue?vue&type=template&id=c9d06050&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/ActionButton/ActionButton.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var ActionButtonvue_type_script_lang_js_ = ({
  components: {
    BaseButton: BaseButton,
    Icon: Icon
  },
  extends: BaseButton,
  props: {
    iconName: {
      type: String,
      default: null
    }
  }
});
// CONCATENATED MODULE: ./src/components/Button/ActionButton/ActionButton.vue?vue&type=script&lang=js&
 /* harmony default export */ var ActionButton_ActionButtonvue_type_script_lang_js_ = (ActionButtonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Button/ActionButton/ActionButton.vue?vue&type=style&index=0&id=c9d06050&lang=scss&scoped=true&
var ActionButtonvue_type_style_index_0_id_c9d06050_lang_scss_scoped_true_ = __webpack_require__("4eac");

// CONCATENATED MODULE: ./src/components/Button/ActionButton/ActionButton.vue






/* normalize component */

var ActionButton_component = normalizeComponent(
  ActionButton_ActionButtonvue_type_script_lang_js_,
  ActionButtonvue_type_template_id_c9d06050_scoped_true_render,
  ActionButtonvue_type_template_id_c9d06050_scoped_true_staticRenderFns,
  false,
  null,
  "c9d06050",
  null
  
)

ActionButton_component.options.__file = "ActionButton.vue"
/* harmony default export */ var ActionButton = (ActionButton_component.exports);
// CONCATENATED MODULE: ./src/components/Button/CommandBarButton/CommandBarButton.vue
var CommandBarButton_render, CommandBarButton_staticRenderFns
var script = {}


/* normalize component */

var CommandBarButton_component = normalizeComponent(
  script,
  CommandBarButton_render,
  CommandBarButton_staticRenderFns,
  false,
  null,
  null,
  null
  
)

CommandBarButton_component.options.__file = "CommandBarButton.vue"
/* harmony default export */ var CommandBarButton = (CommandBarButton_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/CompoundButton/CompoundButton.vue?vue&type=template&id=3ddee351&scoped=true&
var CompoundButtonvue_type_template_id_3ddee351_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('base-button',_vm._b({staticClass:"ms-Button--compound"},'base-button',_vm.$props,false),[_c('div',{staticClass:"ms-Button-textContainer textContainer"},[_c('div',{staticClass:"ms-Button-label label"},[_vm._v("\n      "+_vm._s(_vm.text)+"\n    ")]),_c('div',{staticClass:"ms-Button-description description"},[_vm._v("\n      "+_vm._s(_vm.secondaryText)+"\n    ")])])])}
var CompoundButtonvue_type_template_id_3ddee351_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Button/CompoundButton/CompoundButton.vue?vue&type=template&id=3ddee351&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/CompoundButton/CompoundButton.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var CompoundButtonvue_type_script_lang_js_ = ({
  components: {
    BaseButton: BaseButton
  },
  extends: BaseButton,
  props: {
    secondaryText: {
      type: String,
      default: ''
    }
  }
});
// CONCATENATED MODULE: ./src/components/Button/CompoundButton/CompoundButton.vue?vue&type=script&lang=js&
 /* harmony default export */ var CompoundButton_CompoundButtonvue_type_script_lang_js_ = (CompoundButtonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Button/CompoundButton/CompoundButton.vue?vue&type=style&index=0&id=3ddee351&lang=scss&scoped=true&
var CompoundButtonvue_type_style_index_0_id_3ddee351_lang_scss_scoped_true_ = __webpack_require__("a53f");

// CONCATENATED MODULE: ./src/components/Button/CompoundButton/CompoundButton.vue






/* normalize component */

var CompoundButton_component = normalizeComponent(
  CompoundButton_CompoundButtonvue_type_script_lang_js_,
  CompoundButtonvue_type_template_id_3ddee351_scoped_true_render,
  CompoundButtonvue_type_template_id_3ddee351_scoped_true_staticRenderFns,
  false,
  null,
  "3ddee351",
  null
  
)

CompoundButton_component.options.__file = "CompoundButton.vue"
/* harmony default export */ var CompoundButton = (CompoundButton_component.exports);
// CONCATENATED MODULE: ./src/components/Button/SplitButton/SplitButton.vue
var SplitButton_render, SplitButton_staticRenderFns
var SplitButton_script = {}


/* normalize component */

var SplitButton_component = normalizeComponent(
  SplitButton_script,
  SplitButton_render,
  SplitButton_staticRenderFns,
  false,
  null,
  null,
  null
  
)

SplitButton_component.options.__file = "SplitButton.vue"
/* harmony default export */ var SplitButton = (SplitButton_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/IconButton/IconButton.vue?vue&type=template&id=5897742a&scoped=true&
var IconButtonvue_type_template_id_5897742a_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('base-button',_vm._b({staticClass:"ms-Button--action ms-Button-command"},'base-button',_vm.$props,false),[_c('icon',{staticClass:"icon",attrs:{"icon-name":_vm.iconName}})],1)}
var IconButtonvue_type_template_id_5897742a_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Button/IconButton/IconButton.vue?vue&type=template&id=5897742a&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button/IconButton/IconButton.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//


/* harmony default export */ var IconButtonvue_type_script_lang_js_ = ({
  components: {
    BaseButton: BaseButton,
    Icon: Icon
  },
  extends: BaseButton,
  props: {
    iconName: {
      type: String,
      required: true
    }
  }
});
// CONCATENATED MODULE: ./src/components/Button/IconButton/IconButton.vue?vue&type=script&lang=js&
 /* harmony default export */ var IconButton_IconButtonvue_type_script_lang_js_ = (IconButtonvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Button/IconButton/IconButton.vue?vue&type=style&index=0&id=5897742a&lang=scss&scoped=true&
var IconButtonvue_type_style_index_0_id_5897742a_lang_scss_scoped_true_ = __webpack_require__("c259");

// CONCATENATED MODULE: ./src/components/Button/IconButton/IconButton.vue






/* normalize component */

var IconButton_component = normalizeComponent(
  IconButton_IconButtonvue_type_script_lang_js_,
  IconButtonvue_type_template_id_5897742a_scoped_true_render,
  IconButtonvue_type_template_id_5897742a_scoped_true_staticRenderFns,
  false,
  null,
  "5897742a",
  null
  
)

IconButton_component.options.__file = "IconButton.vue"
/* harmony default export */ var IconButton = (IconButton_component.exports);
// CONCATENATED MODULE: ./src/components/Button/index.js
// import BaseButton from './BaseButton.vue'
 // import Button from './Button.vue'

 // import BarButton from './CommandBarButton/CommandBarButton.vue'



 // import PrimaryButton from './PrimaryButton/PrimaryButton.vue'



// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Checkbox/Checkbox.vue?vue&type=template&id=fc1d4600&scoped=true&
var Checkboxvue_type_template_id_fc1d4600_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{staticClass:"ms-Checkbox root",class:_vm.classObj,attrs:{"id":_vm.id,"role":"checkbox","type":"button"},on:{"click":function($event){$event.preventDefault();$event.stopPropagation();return _vm.toggleChecked($event)}}},[_c('Label',{staticClass:"ms-Checkbox-label label",attrs:{"for":_vm.id}},[(_vm.boxSide === 'end')?_c('span',{staticClass:"ms-Checkbox-text text"},[_vm._v("\n      "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_c('div',{staticClass:"ms-Checkbox-checkbox checkbox"},[_c('icon',{attrs:{"icon-name":"CheckMark"}})],1),(_vm.boxSide === 'start')?_c('span',{staticClass:"ms-Checkbox-text text"},[_vm._v("\n      "+_vm._s(_vm.label)+"\n    ")]):_vm._e()])],1)}
var Checkboxvue_type_template_id_fc1d4600_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Checkbox/Checkbox.vue?vue&type=template&id=fc1d4600&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Label/Label.vue?vue&type=template&id=7c809f01&scoped=true&
var Labelvue_type_template_id_7c809f01_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{staticClass:"ms-Label root",attrs:{"disabled":_vm.disabled,"required":_vm.required}},[_vm._t("default")],2)}
var Labelvue_type_template_id_7c809f01_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Label/Label.vue?vue&type=template&id=7c809f01&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Label/Label.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
/* harmony default export */ var Labelvue_type_script_lang_js_ = ({
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    required: {
      type: Boolean,
      default: false
    }
  }
});
// CONCATENATED MODULE: ./src/components/Label/Label.vue?vue&type=script&lang=js&
 /* harmony default export */ var Label_Labelvue_type_script_lang_js_ = (Labelvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Label/Label.vue?vue&type=style&index=0&id=7c809f01&lang=scss&scoped=true&
var Labelvue_type_style_index_0_id_7c809f01_lang_scss_scoped_true_ = __webpack_require__("eb1a");

// CONCATENATED MODULE: ./src/components/Label/Label.vue






/* normalize component */

var Label_component = normalizeComponent(
  Label_Labelvue_type_script_lang_js_,
  Labelvue_type_template_id_7c809f01_scoped_true_render,
  Labelvue_type_template_id_7c809f01_scoped_true_staticRenderFns,
  false,
  null,
  "7c809f01",
  null
  
)

Label_component.options.__file = "Label.vue"
/* harmony default export */ var Label = (Label_component.exports);
// CONCATENATED MODULE: ./src/components/Label/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Checkbox/Checkbox.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var Checkboxvue_type_script_lang_js_ = ({
  components: {
    Icon: Icon,
    Label: Label
  },
  props: {
    label: {
      type: String,
      default: ''
    },
    boxSide: {
      type: String,
      default: 'start',
      validator: function validator(v) {
        return v === 'start' || v === 'end';
      }
    },
    checked: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      isChecked: this.checked,
      isDisabled: this.disabled
    };
  },
  computed: {
    id: function id() {
      return 'Checkbox' + this._uid;
    },
    classObj: function classObj() {
      var obj = {};
      obj['is-checked'] = this.isChecked;
      obj['is-enabled'] = !this.isDisabled;
      obj['is-disabled'] = this.isDisabled;
      return obj;
    }
  },
  methods: {
    toggleChecked: function toggleChecked() {
      if (this.isDisabled) return;
      this.isChecked = !this.isChecked;
      this.$emit('onChange', this.isChecked);
    }
  }
});
// CONCATENATED MODULE: ./src/components/Checkbox/Checkbox.vue?vue&type=script&lang=js&
 /* harmony default export */ var Checkbox_Checkboxvue_type_script_lang_js_ = (Checkboxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Checkbox/Checkbox.vue?vue&type=style&index=0&id=fc1d4600&lang=scss&scoped=true&
var Checkboxvue_type_style_index_0_id_fc1d4600_lang_scss_scoped_true_ = __webpack_require__("12fb");

// CONCATENATED MODULE: ./src/components/Checkbox/Checkbox.vue






/* normalize component */

var Checkbox_component = normalizeComponent(
  Checkbox_Checkboxvue_type_script_lang_js_,
  Checkboxvue_type_template_id_fc1d4600_scoped_true_render,
  Checkboxvue_type_template_id_fc1d4600_scoped_true_staticRenderFns,
  false,
  null,
  "fc1d4600",
  null
  
)

Checkbox_component.options.__file = "Checkbox.vue"
/* harmony default export */ var Checkbox = (Checkbox_component.exports);
// CONCATENATED MODULE: ./src/components/Checkbox/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/CommandBar/CommandBar.vue?vue&type=template&id=073452f4&scoped=true&
var CommandBarvue_type_template_id_073452f4_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-FocusZone ms-CommandBar root",attrs:{"role":"menubar"}},[_c('div',{staticClass:"ms-CommandBar-primaryCommands primarySet"},_vm._l((_vm.items),function(item,index){return _c('div',{key:item.key || index,staticClass:"ms-CommandBarItem item"},[_c('action-button',{attrs:{"icon-name":item.icon},nativeOn:{"click":function($event){_vm.onClick($event, item)}}},[_vm._v("\n        "+_vm._s(item.name)+"\n      ")])],1)})),_c('div',{staticClass:"ms-CommandBar-secondaryCommand secondarySet"},_vm._l((_vm.farItems),function(item,index){return _c('div',{key:item.key || index,staticClass:"ms-CommandBarItem item"},[_c('action-button',{attrs:{"icon-name":item.icon},nativeOn:{"click":function($event){_vm.onClick($event, item)}}},[_vm._v("\n        "+_vm._s(item.name)+"\n      ")])],1)}))])}
var CommandBarvue_type_template_id_073452f4_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/CommandBar/CommandBar.vue?vue&type=template&id=073452f4&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/SearchBox/SearchBox.vue?vue&type=template&id=1256c1e4&scoped=true&
var SearchBoxvue_type_template_id_1256c1e4_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-SearchBox-container",class:_vm.className},[_c('div',{staticClass:"ms-SearchBox root",class:_vm.classObj},[_c('div',{staticClass:"ms-SearchBox-iconContainer iconContainer"},[_c('icon',{attrs:{"icon-name":"Search"}})],1),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.searchQuery),expression:"searchQuery"}],ref:"searchInput",staticClass:"ms-SearchBox-field field",attrs:{"placeholder":_vm.placeholder,"type":"text"},domProps:{"value":(_vm.searchQuery)},on:{"blur":_vm.onBlur,"focus":_vm.onFocus,"keyup":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }$event.preventDefault();return _vm.onSearch($event)},"keydown":function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"enter",13,$event.key,"Enter")){ return null; }$event.preventDefault();$event.stopPropagation();},"input":function($event){if($event.target.composing){ return; }_vm.searchQuery=$event.target.value}}}),_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.hasText),expression:"hasText"}],staticClass:"ms-SearchBox-clearButton clearButton",on:{"click":_vm.onClear}},[_c('icon',{attrs:{"icon-name":"Clear"}})],1),(_vm.clear)?_c('div',{staticStyle:{"clear":"both"}}):_vm._e()])])}
var SearchBoxvue_type_template_id_1256c1e4_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/SearchBox/SearchBox.vue?vue&type=template&id=1256c1e4&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/SearchBox/SearchBox.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var SearchBoxvue_type_script_lang_js_ = ({
  components: {
    Icon: Icon
  },
  props: {
    className: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: 'Search'
    },
    value: {
      type: String,
      default: ''
    },
    underlined: {
      type: Boolean,
      default: false
    },
    clear: {
      type: Boolean,
      default: true
    }
  },
  data: function data() {
    return {
      isActive: false,
      searchQuery: this.value
    };
  },
  computed: {
    hasText: function hasText() {
      return this.searchQuery && this.searchQuery.length > 0;
    },
    isDisabled: function isDisabled() {
      return this.$attrs.disabled != null;
    },
    classObj: function classObj() {
      var obj = {};
      obj['is-disabled'] = this.isDisabled;
      obj['is-underlined'] = this.underlined;
      obj['has-text'] = this.hasText;
      obj['can-clear'] = this.hasText;
      obj['is-active'] = this.isActive;
      return obj;
    }
  },
  watch: {
    searchQuery: function searchQuery() {
      this.onChange();
    }
  },
  methods: {
    onClear: function onClear() {
      this.searchQuery = '';
      this.$refs.searchInput.focus();
    },
    onBlur: function onBlur() {
      this.isActive = false;
      this.$emit('onBlur', this.searchQuery);
    },
    onFocus: function onFocus() {
      this.isActive = !this.isDisabled;
      this.$emit('onFocus', this.searchQuery);
    },
    onChange: function onChange() {
      this.$emit('onChange', this.searchQuery);
    },
    onSearch: function onSearch() {
      this.$emit('onSearch', this.searchQuery);
    }
  }
});
// CONCATENATED MODULE: ./src/components/SearchBox/SearchBox.vue?vue&type=script&lang=js&
 /* harmony default export */ var SearchBox_SearchBoxvue_type_script_lang_js_ = (SearchBoxvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/SearchBox/SearchBox.vue?vue&type=style&index=0&id=1256c1e4&lang=scss&scoped=true&
var SearchBoxvue_type_style_index_0_id_1256c1e4_lang_scss_scoped_true_ = __webpack_require__("bafc");

// CONCATENATED MODULE: ./src/components/SearchBox/SearchBox.vue






/* normalize component */

var SearchBox_component = normalizeComponent(
  SearchBox_SearchBoxvue_type_script_lang_js_,
  SearchBoxvue_type_template_id_1256c1e4_scoped_true_render,
  SearchBoxvue_type_template_id_1256c1e4_scoped_true_staticRenderFns,
  false,
  null,
  "1256c1e4",
  null
  
)

SearchBox_component.options.__file = "SearchBox.vue"
/* harmony default export */ var SearchBox = (SearchBox_component.exports);
// CONCATENATED MODULE: ./src/components/SearchBox/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/CommandBar/CommandBar.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var CommandBarvue_type_script_lang_js_ = ({
  components: {
    ActionButton: ActionButton,
    Icon: Icon,
    SearchBox: SearchBox
  },
  props: {
    items: {
      type: Array,
      default: function _default() {
        return [];
      },
      dummy: function dummy() {
        return [{
          key: 'new',
          name: 'New',
          icon: 'Add',
          href: '#',
          onClick: function onClick() {}
        }];
      }
    },
    farItems: {
      type: Array,
      default: function _default() {
        return [];
      },
      dummy: function dummy() {
        return [{
          key: 'sort',
          name: 'Sort',
          icon: 'SortLines',
          href: '#',
          onClick: function onClick() {}
        }];
      }
    },
    isSearchBoxVisible: {
      type: Boolean,
      default: false
    },
    searchPlaceholderText: {
      type: String,
      default: 'Search...'
    }
  },
  created: function created() {},
  methods: {
    onClick: function onClick(event, item) {
      if (!item.onClick) return;
      item.onClick(event);
    }
  }
});
// CONCATENATED MODULE: ./src/components/CommandBar/CommandBar.vue?vue&type=script&lang=js&
 /* harmony default export */ var CommandBar_CommandBarvue_type_script_lang_js_ = (CommandBarvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/CommandBar/CommandBar.vue?vue&type=style&index=0&id=073452f4&lang=scss&scoped=true&
var CommandBarvue_type_style_index_0_id_073452f4_lang_scss_scoped_true_ = __webpack_require__("1bb5");

// CONCATENATED MODULE: ./src/components/CommandBar/CommandBar.vue






/* normalize component */

var CommandBar_component = normalizeComponent(
  CommandBar_CommandBarvue_type_script_lang_js_,
  CommandBarvue_type_template_id_073452f4_scoped_true_render,
  CommandBarvue_type_template_id_073452f4_scoped_true_staticRenderFns,
  false,
  null,
  "073452f4",
  null
  
)

CommandBar_component.options.__file = "CommandBar.vue"
/* harmony default export */ var CommandBar = (CommandBar_component.exports);
// CONCATENATED MODULE: ./src/components/CommandBar/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ContextualMenu/ContextualMenu.vue?vue&type=template&id=2036b2b1&
var ContextualMenuvue_type_template_id_2036b2b1_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',_vm._l((_vm.items),function(item){return _c('contextual-menu-item',{key:item.key,attrs:{"text":item.text}})}))}
var ContextualMenuvue_type_template_id_2036b2b1_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenu.vue?vue&type=template&id=2036b2b1&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ContextualMenu/ContextualMenuItem.vue?vue&type=template&id=34d8a7e8&
var ContextualMenuItemvue_type_template_id_34d8a7e8_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._v(_vm._s(_vm.text))])}
var ContextualMenuItemvue_type_template_id_34d8a7e8_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenuItem.vue?vue&type=template&id=34d8a7e8&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ContextualMenu/ContextualMenuItem.vue?vue&type=script&lang=js&
//
//
//
//
/* harmony default export */ var ContextualMenuItemvue_type_script_lang_js_ = ({
  props: {
    text: {
      type: String,
      required: true
    }
  }
});
// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenuItem.vue?vue&type=script&lang=js&
 /* harmony default export */ var ContextualMenu_ContextualMenuItemvue_type_script_lang_js_ = (ContextualMenuItemvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenuItem.vue





/* normalize component */

var ContextualMenuItem_component = normalizeComponent(
  ContextualMenu_ContextualMenuItemvue_type_script_lang_js_,
  ContextualMenuItemvue_type_template_id_34d8a7e8_render,
  ContextualMenuItemvue_type_template_id_34d8a7e8_staticRenderFns,
  false,
  null,
  null,
  null
  
)

ContextualMenuItem_component.options.__file = "ContextualMenuItem.vue"
/* harmony default export */ var ContextualMenuItem = (ContextualMenuItem_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ContextualMenu/ContextualMenu.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//

/* harmony default export */ var ContextualMenuvue_type_script_lang_js_ = ({
  components: {
    ContextualMenuItem: ContextualMenuItem
  },
  props: {
    items: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenu.vue?vue&type=script&lang=js&
 /* harmony default export */ var ContextualMenu_ContextualMenuvue_type_script_lang_js_ = (ContextualMenuvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/ContextualMenu/ContextualMenu.vue?vue&type=style&index=0&lang=css&
var ContextualMenuvue_type_style_index_0_lang_css_ = __webpack_require__("c629");

// CONCATENATED MODULE: ./src/components/ContextualMenu/ContextualMenu.vue






/* normalize component */

var ContextualMenu_component = normalizeComponent(
  ContextualMenu_ContextualMenuvue_type_script_lang_js_,
  ContextualMenuvue_type_template_id_2036b2b1_render,
  ContextualMenuvue_type_template_id_2036b2b1_staticRenderFns,
  false,
  null,
  null,
  null
  
)

ContextualMenu_component.options.__file = "ContextualMenu.vue"
/* harmony default export */ var ContextualMenu = (ContextualMenu_component.exports);
// CONCATENATED MODULE: ./src/components/ContextualMenu/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Dropdown/Dropdown.vue?vue&type=template&id=bbbc04c8&scoped=true&
var Dropdownvue_type_template_id_bbbc04c8_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._v("test")])}
var Dropdownvue_type_template_id_bbbc04c8_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Dropdown/Dropdown.vue?vue&type=template&id=bbbc04c8&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Dropdown/Dropdown.vue?vue&type=script&lang=js&
//
//
//
//
/* harmony default export */ var Dropdownvue_type_script_lang_js_ = ({});
// CONCATENATED MODULE: ./src/components/Dropdown/Dropdown.vue?vue&type=script&lang=js&
 /* harmony default export */ var Dropdown_Dropdownvue_type_script_lang_js_ = (Dropdownvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Dropdown/Dropdown.vue?vue&type=style&index=0&id=bbbc04c8&lang=scss&scoped=true&
var Dropdownvue_type_style_index_0_id_bbbc04c8_lang_scss_scoped_true_ = __webpack_require__("4fdb");

// CONCATENATED MODULE: ./src/components/Dropdown/Dropdown.vue






/* normalize component */

var Dropdown_component = normalizeComponent(
  Dropdown_Dropdownvue_type_script_lang_js_,
  Dropdownvue_type_template_id_bbbc04c8_scoped_true_render,
  Dropdownvue_type_template_id_bbbc04c8_scoped_true_staticRenderFns,
  false,
  null,
  "bbbc04c8",
  null
  
)

Dropdown_component.options.__file = "Dropdown.vue"
/* harmony default export */ var Dropdown = (Dropdown_component.exports);
// CONCATENATED MODULE: ./src/components/Dropdown/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Link/Link.vue?vue&type=template&id=04588638&scoped=true&
var Linkvue_type_template_id_04588638_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.href)?_c('a',{staticClass:"ms-Link root",class:_vm.classObj,attrs:{"href":_vm.href}},[_vm._t("default")],2):_c('button',{staticClass:"ms-Link root"},[_vm._t("default")],2)}
var Linkvue_type_template_id_04588638_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Link/Link.vue?vue&type=template&id=04588638&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Link/Link.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Linkvue_type_script_lang_js_ = ({
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    href: {
      type: String,
      default: null
    }
  },
  computed: {
    classObj: function classObj() {
      var obj = {};
      obj['isDisabled'] = this.disabled;
      return obj;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Link/Link.vue?vue&type=script&lang=js&
 /* harmony default export */ var Link_Linkvue_type_script_lang_js_ = (Linkvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Link/Link.vue?vue&type=style&index=0&id=04588638&lang=scss&scoped=true&
var Linkvue_type_style_index_0_id_04588638_lang_scss_scoped_true_ = __webpack_require__("7a8f");

// CONCATENATED MODULE: ./src/components/Link/Link.vue






/* normalize component */

var Link_component = normalizeComponent(
  Link_Linkvue_type_script_lang_js_,
  Linkvue_type_template_id_04588638_scoped_true_render,
  Linkvue_type_template_id_04588638_scoped_true_staticRenderFns,
  false,
  null,
  "04588638",
  null
  
)

Link_component.options.__file = "Link.vue"
/* harmony default export */ var Link = (Link_component.exports);
// CONCATENATED MODULE: ./src/components/Link/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Persona/Persona.vue?vue&type=template&id=79af599f&scoped=true&
var Personavue_type_template_id_79af599f_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Persona",attrs:{"size":_vm.size}},[_c('div',{staticClass:"ms-Persona-coin"},[_vm._t("coin",[(_vm.size === 9)?_c('i',{staticClass:"ms-Icon ms-Icon--Contact",attrs:{"aria-hidden":"true"}}):_c('div',{staticClass:"ms-Persona-imageArea"},[(_vm.presence)?_c('div',{staticClass:"ms-Persona-presence"}):_vm._e(),(!_vm.imageUrl)?_c('persona-initials',{attrs:{"name":_vm.primaryText}}):_c('div',{staticClass:"ms-Image ms-Persona-image"},[_c('img',{staticClass:"ms-Image-image",attrs:{"src":_vm.imageUrl,"alt":""}})])],1)])],2),_c('div',{staticClass:"ms-Persona-details"},[_vm._t("details",[_c('div',{staticClass:"ms-Persona-primaryText"},[_c('span',[_vm._v(_vm._s(_vm.primaryText))])])])],2)])}
var Personavue_type_template_id_79af599f_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Persona/Persona.vue?vue&type=template&id=79af599f&scoped=true&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.number.constructor.js
var es6_number_constructor = __webpack_require__("c5f6");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Persona/PersonaInitials.vue?vue&type=template&id=68cf0c08&
var PersonaInitialsvue_type_template_id_68cf0c08_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Persona-initials",style:({ 'background-color': _vm.initialsColor })},[_c('span',[_vm._v(_vm._s(_vm.initials))])])}
var PersonaInitialsvue_type_template_id_68cf0c08_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Persona/PersonaInitials.vue?vue&type=template&id=68cf0c08&

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.regexp.split.js
var es6_regexp_split = __webpack_require__("28a5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es6.function.name.js
var es6_function_name = __webpack_require__("7f7f");

// CONCATENATED MODULE: ./src/components/Persona/initials.js
var COLOR_SWATCHES_LOOKUP = ['lightGreen', 'lightBlue', 'lightPink', 'green', 'darkGreen', 'pink', 'magenta', 'purple', 'black', 'teal', 'blue', 'darkBlue', 'orange', 'darkRed'];
var COLOR_SWATCHES_NUM_ENTRIES = COLOR_SWATCHES_LOOKUP.length;

function getInitialsColorFromName(displayName) {
  var color = 'blue';

  if (!displayName) {
    return color;
  }

  var hashCode = 0;

  for (var iLen = displayName.length - 1; iLen >= 0; iLen--) {
    var ch = displayName.charCodeAt(iLen);
    var shift = iLen % 8; // tslint:disable-next-line:no-bitwise

    hashCode ^= (ch << shift) + (ch >> 8 - shift);
  }

  color = COLOR_SWATCHES_LOOKUP[hashCode % COLOR_SWATCHES_NUM_ENTRIES];
  return color;
}

function personaInitialsColorToHexCode(personaInitialsColor) {
  switch (personaInitialsColor) {
    case 'lightBlue':
      return '#6BA5E7';

    case 'blue':
      return '#2D89EF';

    case 'darkBlue':
      return '#2B5797';

    case 'teal':
      return '#00ABA9';

    case 'lightGreen':
      return '#99B433';

    case 'green':
      return '#00A300';

    case 'darkGreen':
      return '#1E7145';

    case 'lightPink':
      return '#E773BD';

    case 'pink':
      return '#FF0097';

    case 'magenta':
      return ' #7E3878';

    case 'purple':
      return '#603CBA';

    case 'black':
      return '#1D1D1D';

    case 'orange':
      return '#DA532C';

    case 'red':
      return '#EE1111';

    case 'darkRed':
      return '#B91D47';

    case 'transparent':
      return 'transparent';
  }
}

function initialsColorPropToColorCode(props) {
  var primaryText = props.primaryText;
  var initialsColor = props.initialsColor;
  var initialsColorCode;

  if (typeof initialsColor === 'string') {
    initialsColorCode = initialsColor;
  } else {
    initialsColor = initialsColor !== undefined ? initialsColor : getInitialsColorFromName(primaryText);
    initialsColorCode = personaInitialsColorToHexCode(initialsColor);
  }

  return initialsColorCode;
}
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Persona/PersonaInitials.vue?vue&type=script&lang=js&


//
//
//
//
//
//
//

/* harmony default export */ var PersonaInitialsvue_type_script_lang_js_ = ({
  props: {
    name: {
      type: String,
      required: true
    }
  },
  computed: {
    initialsColor: function initialsColor() {
      return initialsColorPropToColorCode({
        primaryText: this.name
      });
    },
    initials: function initials() {
      var name = this.name;
      var split = name.split(' ');
      var first = split[0];
      var last = split[split.length - 1];

      if (split.length > 1) {
        return first[0] + last[0];
      }

      return first[0];
    }
  }
});
// CONCATENATED MODULE: ./src/components/Persona/PersonaInitials.vue?vue&type=script&lang=js&
 /* harmony default export */ var Persona_PersonaInitialsvue_type_script_lang_js_ = (PersonaInitialsvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/components/Persona/PersonaInitials.vue





/* normalize component */

var PersonaInitials_component = normalizeComponent(
  Persona_PersonaInitialsvue_type_script_lang_js_,
  PersonaInitialsvue_type_template_id_68cf0c08_render,
  PersonaInitialsvue_type_template_id_68cf0c08_staticRenderFns,
  false,
  null,
  null,
  null
  
)

PersonaInitials_component.options.__file = "PersonaInitials.vue"
/* harmony default export */ var PersonaInitials = (PersonaInitials_component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Persona/Persona.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var size72 = 14;
/* harmony default export */ var Personavue_type_script_lang_js_ = ({
  components: {
    PersonaInitials: PersonaInitials
  },
  props: {
    imageUrl: {
      type: String,
      default: ''
    },
    primaryText: {
      type: String,
      default: ''
    },
    secondaryText: {
      type: String,
      default: ''
    },
    tertiaryText: {
      type: String,
      default: ''
    },
    size: {
      type: Number,
      default: size72
    },
    presence: {
      type: String,
      default: ''
    }
  }
});
// CONCATENATED MODULE: ./src/components/Persona/Persona.vue?vue&type=script&lang=js&
 /* harmony default export */ var Persona_Personavue_type_script_lang_js_ = (Personavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Persona/Persona.vue?vue&type=style&index=0&id=79af599f&lang=scss&scoped=true&
var Personavue_type_style_index_0_id_79af599f_lang_scss_scoped_true_ = __webpack_require__("67fc");

// CONCATENATED MODULE: ./src/components/Persona/Persona.vue






/* normalize component */

var Persona_component = normalizeComponent(
  Persona_Personavue_type_script_lang_js_,
  Personavue_type_template_id_79af599f_scoped_true_render,
  Personavue_type_template_id_79af599f_scoped_true_staticRenderFns,
  false,
  null,
  "79af599f",
  null
  
)

Persona_component.options.__file = "Persona.vue"
/* harmony default export */ var Persona = (Persona_component.exports);
// CONCATENATED MODULE: ./src/components/Persona/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/OverflowSet/OverflowSet.vue?vue&type=template&id=5c357f62&
var OverflowSetvue_type_template_id_5c357f62_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-OverflowSet root"},[_vm._l((_vm.items),function(item,index){return _c('div',{key:item.key,staticClass:"ms-OverflowSet-item item"},[_c('Link',{staticClass:"overflowLinks",attrs:{"href":item.href}},[_vm._v(_vm._s(item.name))])],1)}),_c('div',{staticClass:"ms-OverflowSet-overflowButton overflowButton"},[_c('icon-button',{staticClass:"overflowButton",attrs:{"icon-name":"More"}})],1)],2)}
var OverflowSetvue_type_template_id_5c357f62_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/OverflowSet/OverflowSet.vue?vue&type=template&id=5c357f62&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/OverflowSet/OverflowSet.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ var OverflowSetvue_type_script_lang_js_ = ({
  components: {
    IconButton: IconButton,
    Link: Link
  },
  props: {
    items: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    overflowItems: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  }
});
// CONCATENATED MODULE: ./src/components/OverflowSet/OverflowSet.vue?vue&type=script&lang=js&
 /* harmony default export */ var OverflowSet_OverflowSetvue_type_script_lang_js_ = (OverflowSetvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/OverflowSet/OverflowSet.vue?vue&type=style&index=0&lang=scss&
var OverflowSetvue_type_style_index_0_lang_scss_ = __webpack_require__("00db");

// CONCATENATED MODULE: ./src/components/OverflowSet/OverflowSet.vue






/* normalize component */

var OverflowSet_component = normalizeComponent(
  OverflowSet_OverflowSetvue_type_script_lang_js_,
  OverflowSetvue_type_template_id_5c357f62_render,
  OverflowSetvue_type_template_id_5c357f62_staticRenderFns,
  false,
  null,
  null,
  null
  
)

OverflowSet_component.options.__file = "OverflowSet.vue"
/* harmony default export */ var OverflowSet = (OverflowSet_component.exports);
// CONCATENATED MODULE: ./src/components/OverflowSet/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ProgressIndicator/ProgressIndicator.vue?vue&type=template&id=8180d784&scoped=true&
var ProgressIndicatorvue_type_template_id_8180d784_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-ProgressIndicator root"},[_c('div',{staticClass:"ms-ProgressIndicator-itemName itemName"},[_vm._v("\n    "+_vm._s(_vm.label)+"\n  ")]),_c('div',{staticClass:"ms-ProgressIndicator-itemProgress itemProgress"},[_c('div',{staticClass:"ms-ProgressIndicator-progressTrack progressTrack"}),_c('div',{staticClass:"ms-ProgressIndicator-progressBar progressBar",class:_vm.classObj,style:({ width: ((_vm.percentComplete < 0 ? 0 : _vm.percentComplete) + "%") })})]),_c('div',{staticClass:"ms-ProgressIndicator-itemDescription itemDescription"},[_vm._v("\n    "+_vm._s(_vm.description)+"\n  ")])])}
var ProgressIndicatorvue_type_template_id_8180d784_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/ProgressIndicator/ProgressIndicator.vue?vue&type=template&id=8180d784&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/ProgressIndicator/ProgressIndicator.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var ProgressIndicatorvue_type_script_lang_js_ = ({
  props: {
    label: {
      type: String,
      default: ''
    },
    description: {
      type: String,
      default: ''
    },
    percentComplete: {
      type: Number,
      default: null
    }
  },
  computed: {
    classObj: function classObj() {
      return {
        indeterminate: this.percentComplete == null,
        smoothTransition: this.percentComplete != null
      };
    }
  }
});
// CONCATENATED MODULE: ./src/components/ProgressIndicator/ProgressIndicator.vue?vue&type=script&lang=js&
 /* harmony default export */ var ProgressIndicator_ProgressIndicatorvue_type_script_lang_js_ = (ProgressIndicatorvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/ProgressIndicator/ProgressIndicator.vue?vue&type=style&index=0&id=8180d784&lang=scss&scoped=true&
var ProgressIndicatorvue_type_style_index_0_id_8180d784_lang_scss_scoped_true_ = __webpack_require__("9d0e");

// CONCATENATED MODULE: ./src/components/ProgressIndicator/ProgressIndicator.vue






/* normalize component */

var ProgressIndicator_component = normalizeComponent(
  ProgressIndicator_ProgressIndicatorvue_type_script_lang_js_,
  ProgressIndicatorvue_type_template_id_8180d784_scoped_true_render,
  ProgressIndicatorvue_type_template_id_8180d784_scoped_true_staticRenderFns,
  false,
  null,
  "8180d784",
  null
  
)

ProgressIndicator_component.options.__file = "ProgressIndicator.vue"
/* harmony default export */ var ProgressIndicator = (ProgressIndicator_component.exports);
// CONCATENATED MODULE: ./src/components/ProgressIndicator/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Shimmer/Shimmer.vue?vue&type=template&id=0cff7e00&scoped=true&
var Shimmervue_type_template_id_0cff7e00_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Shimmer-container root"},[_c('div',{staticClass:"ms-Shimmer-shimmerWrapper shimmerWrapper"},[_c('div',{staticClass:"ms-ShimmerElementsGroup root"},[_c('div',{staticClass:"ms-ShimmerLine-root root",staticStyle:{"width":"100%","min-width":"auto"}},[_c('svg',{staticClass:"ms-ShimmerLine-topLeftCorner topLeftCorner",attrs:{"width":"2","height":"2"}},[_c('path',{attrs:{"d":"M0 2 A 2 2, 0, 0, 1, 2 0 L 0 0 Z"}})]),_c('svg',{staticClass:"ms-ShimmerLine-topRightCorner topRightCorner",attrs:{"width":"2","height":"2"}},[_c('path',{attrs:{"d":"M0 0 A 2 2, 0, 0, 1, 2 2 L 2 0 Z"}})]),_c('svg',{staticClass:"ms-ShimmerLine-bottomRightCorner bottomRightCorner",attrs:{"width":"2","height":"2"}},[_c('path',{attrs:{"d":"M2 0 A 2 2, 0, 0, 1, 0 2 L 2 2 Z"}})]),_c('svg',{staticClass:"ms-ShimmerLine-bottomLeftCorner bottomLeftCorner",attrs:{"width":"2","height":"2"}},[_c('path',{attrs:{"d":"M2 2 A 2 2, 0, 0, 1, 0 0 L 0 2 Z"}})])])])])])}
var Shimmervue_type_template_id_0cff7e00_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Shimmer/Shimmer.vue?vue&type=template&id=0cff7e00&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Shimmer/Shimmer.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Shimmervue_type_script_lang_js_ = ({});
// CONCATENATED MODULE: ./src/components/Shimmer/Shimmer.vue?vue&type=script&lang=js&
 /* harmony default export */ var Shimmer_Shimmervue_type_script_lang_js_ = (Shimmervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Shimmer/Shimmer.vue?vue&type=style&index=0&id=0cff7e00&lang=scss&scoped=true&
var Shimmervue_type_style_index_0_id_0cff7e00_lang_scss_scoped_true_ = __webpack_require__("3bf0");

// CONCATENATED MODULE: ./src/components/Shimmer/Shimmer.vue






/* normalize component */

var Shimmer_component = normalizeComponent(
  Shimmer_Shimmervue_type_script_lang_js_,
  Shimmervue_type_template_id_0cff7e00_scoped_true_render,
  Shimmervue_type_template_id_0cff7e00_scoped_true_staticRenderFns,
  false,
  null,
  "0cff7e00",
  null
  
)

Shimmer_component.options.__file = "Shimmer.vue"
/* harmony default export */ var Shimmer = (Shimmer_component.exports);
// CONCATENATED MODULE: ./src/components/Shimmer/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Spinner/Spinner.vue?vue&type=template&id=40312040&scoped=true&
var Spinnervue_type_template_id_40312040_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Spinner root"},[_c('div',{staticClass:"ms-Spinner-circle circle",class:_vm.classObj,style:(_vm.styleObj)}),(_vm.size >= 2)?_c('div',{staticClass:"ms-Spinner-label label"},[_vm._v("\n    "+_vm._s(_vm.label)+"\n  ")]):_vm._e()])}
var Spinnervue_type_template_id_40312040_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Spinner/Spinner.vue?vue&type=template&id=40312040&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Spinner/Spinner.vue?vue&type=script&lang=js&


//
//
//
//
//
//
//
//
//
//
//
//
var SPINNER_SIZE = {
  0: {
    size: '12px',
    name: 'xSmall'
  },
  1: {
    size: '16px',
    name: 'small'
  },
  2: {
    size: '20px',
    name: 'Medium'
  },
  3: {
    size: '28px',
    name: 'Large'
  }
};
/* harmony default export */ var Spinnervue_type_script_lang_js_ = ({
  props: {
    label: {
      type: String,
      default: null
    },
    size: {
      type: Number,
      default: 2,
      validator: function validator(value) {
        return SPINNER_SIZE[value] != null;
      }
    }
  },
  computed: {
    classObj: function classObj() {
      var size = SPINNER_SIZE[this.size];
      var obj = {};
      obj["ms-Spinner--".concat(size.name)] = true;
      return obj;
    },
    styleObj: function styleObj() {
      var size = SPINNER_SIZE[this.size];
      var style = {};
      style.width = size.size;
      style.height = size.size;
      return style;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Spinner/Spinner.vue?vue&type=script&lang=js&
 /* harmony default export */ var Spinner_Spinnervue_type_script_lang_js_ = (Spinnervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Spinner/Spinner.vue?vue&type=style&index=0&id=40312040&lang=scss&scoped=true&
var Spinnervue_type_style_index_0_id_40312040_lang_scss_scoped_true_ = __webpack_require__("135b");

// CONCATENATED MODULE: ./src/components/Spinner/Spinner.vue






/* normalize component */

var Spinner_component = normalizeComponent(
  Spinner_Spinnervue_type_script_lang_js_,
  Spinnervue_type_template_id_40312040_scoped_true_render,
  Spinnervue_type_template_id_40312040_scoped_true_staticRenderFns,
  false,
  null,
  "40312040",
  null
  
)

Spinner_component.options.__file = "Spinner.vue"
/* harmony default export */ var Spinner = (Spinner_component.exports);
// CONCATENATED MODULE: ./src/components/Spinner/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Slider/Slider.vue?vue&type=template&id=7830d5ff&scoped=true&
var Slidervue_type_template_id_7830d5ff_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Slider root ms-Slider-enabled",class:_vm.rootClass},[(_vm.label)?_c('label',{staticClass:"ms-Label titleLabel root",attrs:{"for":_vm.id}},[_vm._v("\n    "+_vm._s(_vm.label)+"\n  ")]):_vm._e(),_c('div',{staticClass:"ms-Slider-container container"},[_c('button',{staticClass:"ms-Slider-slideBox slideBox ms-Slider-showValue ms-Slider-showTransitions showTransitions",attrs:{"id":_vm.id,"type":"button","role":"slider"},on:{"mousedown":_vm.onMouseDown,"touchstart":_vm.onTouchStart,"mouseup":_vm.onMouseUp,"touchend":_vm.onTouchEnd,"keydown":[function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"up",38,$event.key,["Up","ArrowUp"])){ return null; }_vm.setValue(1)},function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"right",39,$event.key,["Right","ArrowRight"])){ return null; }if('button' in $event && $event.button !== 2){ return null; }_vm.setValue(1)},function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"down",40,$event.key,["Down","ArrowDown"])){ return null; }_vm.setValue(-1)},function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"left",37,$event.key,["Left","ArrowLeft"])){ return null; }if('button' in $event && $event.button !== 0){ return null; }_vm.setValue(-1)},function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"home",undefined,$event.key,undefined)){ return null; }_vm.setValue(0, _vm.min)},function($event){if(!('button' in $event)&&_vm._k($event.keyCode,"end",undefined,$event.key,undefined)){ return null; }_vm.setValue(0, _vm.max)}]}},[_c('div',{ref:"sliderLine",staticClass:"ms-Slider-line line"},[_c('span',{staticClass:"ms-Slider-thumb thumb",style:(_vm.thumbStyle)}),_c('span',{staticClass:"ms-Slider-active lineContainer activeSection",style:(_vm.activeStyle)}),_c('span',{staticClass:"ms-Slider-inactive lineContainer inactiveSection",style:(_vm.inactiveStyle)})])]),(_vm.showValue)?_c('label',{staticClass:"ms-Label ms-Slider-value valueLabel root"},[_vm._v("\n      "+_vm._s(_vm.value)+"\n    ")]):_vm._e()])])}
var Slidervue_type_template_id_7830d5ff_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Slider/Slider.vue?vue&type=template&id=7830d5ff&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Slider/Slider.vue?vue&type=script&lang=js&

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var Slidervue_type_script_lang_js_ = ({
  props: {
    label: {
      type: String,
      default: null
    },
    disabled: {
      type: Boolean,
      default: false
    },
    defaultValue: {
      type: Number,
      default: 5
    },
    min: {
      type: Number,
      default: 0
    },
    max: {
      type: Number,
      default: 10
    },
    step: {
      type: Number,
      default: 1
    },
    showValue: {
      type: Boolean,
      default: true
    },
    vertical: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      isDragging: false,
      value: this.defaultValue,
      renderedValue: this.defaultValue
    };
  },
  computed: {
    id: function id() {
      return 'Slider' + this._uid;
    },
    rootClass: function rootClass() {
      var obj = {
        'ms-Slider-row': !this.vertical,
        'rootIsHorizontal': !this.vertical,
        'ms-Slider-column': this.vertical,
        'rootIsVertical': this.vertical,
        'rootIsEnabled': !this.disabled,
        'rootIsDisabled': this.disabled
      };
      return obj;
    },
    activeStyle: function activeStyle() {
      var obj = {};
      obj[this.vertical ? 'height' : 'width'] = this.thumbOffsetPercent + '%';
      return obj;
    },
    inactiveStyle: function inactiveStyle() {
      var obj = {};
      obj[this.vertical ? 'height' : 'width'] = 100 - this.thumbOffsetPercent + '%';
      return obj;
    },
    thumbOffsetPercent: function thumbOffsetPercent() {
      var value = this.isDragging ? this.renderedValue : this.value;
      return (value - this.min) / (this.max - this.min) * 100;
    },
    thumbStyle: function thumbStyle() {
      var obj = {};
      obj[this.vertical ? 'bottom' : 'left'] = this.thumbOffsetPercent + '%';
      return obj;
    }
  },
  methods: {
    onMouseDown: function onMouseDown() {
      if (this.disabled) return;
      this.isDragging = true;
      window.addEventListener('mousemove', this.onMove);
      window.addEventListener('mouseup', this.onMouseUp);
    },
    onMouseUp: function onMouseUp() {
      this.isDragging = false;
      window.removeEventListener('mousemove', this.onMove);
      window.addEventListener('mouseup', this.onMouseUp);
    },
    onTouchStart: function onTouchStart() {
      if (this.disabled) return;
      this.isDragging = true;
      window.addEventListener('touchmove', this.onMove);
      window.addEventListener('touchend', this.onMouseUp);
    },
    onTouchEnd: function onTouchEnd() {
      this.isDragging = false;
      window.removeEventListener('touchmove', this.onMove);
      window.addEventListener('touchend', this.onMouseUp);
    },
    onMove: function onMove(event) {
      var steps = (this.max - this.min) / this.step;
      var sliderPositionRect = this.$refs.sliderLine.getBoundingClientRect();
      var sliderLength = !this.vertical ? sliderPositionRect.width : sliderPositionRect.height;
      var stepLength = sliderLength / steps;
      var currentSteps;
      var distance;

      if (!this.vertical) {
        var left = this.getPosition(event);
        distance = left - sliderPositionRect.left;
        currentSteps = distance / stepLength;
      } else {
        var bottom = this.getPosition(event);
        distance = sliderPositionRect.bottom - bottom;
        currentSteps = distance / stepLength;
      }

      var currentValue;
      var renderedValue;

      if (currentSteps > Math.floor(steps)) {
        renderedValue = currentValue = this.max;
      } else if (currentSteps < 0) {
        renderedValue = currentValue = this.min;
      } else {
        renderedValue = this.min + this.step * currentSteps;
        currentValue = this.min + this.step * Math.round(currentSteps);
      }

      this.value = currentValue;
      this.renderedValue = renderedValue;
      event.preventDefault();
      event.stopPropagation();
    },
    getPosition: function getPosition(event) {
      var currentPosition = 0;

      switch (event.type) {
        case 'mousedown':
        case 'mousemove':
          currentPosition = !this.vertical ? event.clientX : event.clientY;
          break;

        case 'touchstart':
        case 'touchmove':
          currentPosition = !this.vertical ? event.touches[0].clientX : event.touches[0].clientY;
          break;
      }

      return currentPosition;
    },
    setValue: function setValue(diff) {
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.value;
      var newValue = Math.min(this.max, Math.max(this.min, value + diff));
      this.value = newValue;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Slider/Slider.vue?vue&type=script&lang=js&
 /* harmony default export */ var Slider_Slidervue_type_script_lang_js_ = (Slidervue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Slider/Slider.vue?vue&type=style&index=0&id=7830d5ff&lang=scss&scoped=true&
var Slidervue_type_style_index_0_id_7830d5ff_lang_scss_scoped_true_ = __webpack_require__("a387");

// CONCATENATED MODULE: ./src/components/Slider/Slider.vue






/* normalize component */

var Slider_component = normalizeComponent(
  Slider_Slidervue_type_script_lang_js_,
  Slidervue_type_template_id_7830d5ff_scoped_true_render,
  Slidervue_type_template_id_7830d5ff_scoped_true_staticRenderFns,
  false,
  null,
  "7830d5ff",
  null
  
)

Slider_component.options.__file = "Slider.vue"
/* harmony default export */ var Slider = (Slider_component.exports);
// CONCATENATED MODULE: ./src/components/Slider/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/TextField/TextField.vue?vue&type=template&id=b28a83c4&scoped=true&
var TextFieldvue_type_template_id_b28a83c4_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-TextField root",class:_vm.rootClass},[_c('div',{staticClass:"ms-TextField-wrapper wrapper"},[(_vm.label)?_c('Label',{attrs:{"required":_vm.required}},[_vm._v("\n      "+_vm._s(_vm.label)+"\n    ")]):_vm._e(),_c('div',{staticClass:"ms-TextField-fieldGroup fieldGroup",class:{ 'invalid': _vm.errorMessage }},[_c(_vm.component,{tag:"component",staticClass:"ms-TextField-field field",attrs:{"id":"TextField","value":_vm.multiline ? '' : _vm.value,"disabled":_vm.disabled,"placeholder":_vm.placeholder,"readonly":_vm.readOnly,"required":_vm.required,"rows":"4","type":"text","aria-invalid":"false"},domProps:{"textContent":_vm._s(_vm.multiline ? _vm.value : '')}})],1),_c('span',[_c('div',{attrs:{"aria-live":"assertive"}},[_c('p',{staticClass:"ms-TextField-errorMessage errorMessage"},[_c('span',[_vm._v(_vm._s(_vm.errorMessage))])])])])],1)])}
var TextFieldvue_type_template_id_b28a83c4_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/TextField/TextField.vue?vue&type=template&id=b28a83c4&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/TextField/TextField.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var TextFieldvue_type_script_lang_js_ = ({
  components: {
    Label: Label
  },
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    readOnly: {
      type: Boolean,
      default: false
    },
    required: {
      type: Boolean,
      default: false
    },
    errorMessage: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: ''
    },
    multiline: {
      type: Boolean,
      default: false
    },
    underlined: {
      type: Boolean,
      default: false
    },
    label: {
      type: String,
      default: ''
    },
    value: {
      type: String,
      default: ''
    }
  },
  computed: {
    component: function component() {
      return this.multiline ? 'textarea' : 'input';
    },
    rootClass: function rootClass() {
      return {
        'is-disabled': this.disabled,
        'rootIsDisabled': this.disabled,
        'is-required': this.required,
        'rootIsRequiredLabel': this.required,
        'multiline': this.multiline,
        'rootIsMultiline': this.multiline,
        'underlined': this.underlined,
        'rootIsUnderlined': this.underlined
      };
    }
  }
});
// CONCATENATED MODULE: ./src/components/TextField/TextField.vue?vue&type=script&lang=js&
 /* harmony default export */ var TextField_TextFieldvue_type_script_lang_js_ = (TextFieldvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/TextField/TextField.vue?vue&type=style&index=0&id=b28a83c4&lang=scss&scoped=true&
var TextFieldvue_type_style_index_0_id_b28a83c4_lang_scss_scoped_true_ = __webpack_require__("fc6b");

// CONCATENATED MODULE: ./src/components/TextField/TextField.vue






/* normalize component */

var TextField_component = normalizeComponent(
  TextField_TextFieldvue_type_script_lang_js_,
  TextFieldvue_type_template_id_b28a83c4_scoped_true_render,
  TextFieldvue_type_template_id_b28a83c4_scoped_true_staticRenderFns,
  false,
  null,
  "b28a83c4",
  null
  
)

TextField_component.options.__file = "TextField.vue"
/* harmony default export */ var TextField = (TextField_component.exports);
// CONCATENATED MODULE: ./src/components/TextField/index.js


// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules//.cache//vue-loader","cacheIdentifier":"0595ba96-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Toggle/Toggle.vue?vue&type=template&id=986803ca&scoped=true&
var Togglevue_type_template_id_986803ca_scoped_true_render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"ms-Toggle root",class:_vm.classObj},[(_vm.label)?_c('Label',{attrs:{"for":_vm.id}},[_vm._v("\n    "+_vm._s(_vm.label)+"\n  ")]):_vm._e(),_c('div',{staticClass:"ms-Toggle-innerContainer container"},[_c('button',{staticClass:"ms-Toggle-background pill",attrs:{"id":_vm.id},on:{"click":_vm.onClick}},[_c('div',{staticClass:"ms-Toggle-thumb thumb"})]),(_vm.onText || _vm.offText)?_c('Label',{staticClass:"ms-Toggle-stateText text",attrs:{"for":_vm.id}},[_vm._v("\n      "+_vm._s(_vm.isActive ? _vm.onText : _vm.offText)+"\n    ")]):_vm._e()],1)],1)}
var Togglevue_type_template_id_986803ca_scoped_true_staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Toggle/Toggle.vue?vue&type=template&id=986803ca&scoped=true&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Toggle/Toggle.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ var Togglevue_type_script_lang_js_ = ({
  components: {
    Label: Label
  },
  props: {
    checked: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    label: {
      type: String,
      default: ''
    },
    onText: {
      type: String,
      default: ''
    },
    offText: {
      type: String,
      default: ''
    }
  },
  data: function data() {
    return {
      isActive: this.checked
    };
  },
  computed: {
    id: function id() {
      return 'Toggle' + this._uid;
    },
    classObj: function classObj() {
      var obj = {};
      obj['is-active'] = this.isActive;
      obj['is-disabled'] = this.disabled;
      return obj;
    }
  },
  methods: {
    onClick: function onClick() {
      if (this.disabled) return;
      this.isActive = !this.isActive;
    }
  }
});
// CONCATENATED MODULE: ./src/components/Toggle/Toggle.vue?vue&type=script&lang=js&
 /* harmony default export */ var Toggle_Togglevue_type_script_lang_js_ = (Togglevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Toggle/Toggle.vue?vue&type=style&index=0&id=986803ca&lang=scss&scoped=true&
var Togglevue_type_style_index_0_id_986803ca_lang_scss_scoped_true_ = __webpack_require__("1c34");

// CONCATENATED MODULE: ./src/components/Toggle/Toggle.vue






/* normalize component */

var Toggle_component = normalizeComponent(
  Toggle_Togglevue_type_script_lang_js_,
  Togglevue_type_template_id_986803ca_scoped_true_render,
  Togglevue_type_template_id_986803ca_scoped_true_staticRenderFns,
  false,
  null,
  "986803ca",
  null
  
)

Toggle_component.options.__file = "Toggle.vue"
/* harmony default export */ var Toggle = (Toggle_component.exports);
// CONCATENATED MODULE: ./src/components/Toggle/index.js


// CONCATENATED MODULE: ./src/index.js


















// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib-no-default.js
/* concated harmony reexport Breadcrumb */__webpack_require__.d(__webpack_exports__, "Breadcrumb", function() { return Breadcrumb; });
/* concated harmony reexport ActionButton */__webpack_require__.d(__webpack_exports__, "ActionButton", function() { return ActionButton; });
/* concated harmony reexport DefaultButton */__webpack_require__.d(__webpack_exports__, "DefaultButton", function() { return DefaultButton; });
/* concated harmony reexport CompoundButton */__webpack_require__.d(__webpack_exports__, "CompoundButton", function() { return CompoundButton; });
/* concated harmony reexport CommandBarButton */__webpack_require__.d(__webpack_exports__, "CommandBarButton", function() { return CommandBarButton; });
/* concated harmony reexport SplitButton */__webpack_require__.d(__webpack_exports__, "SplitButton", function() { return SplitButton; });
/* concated harmony reexport IconButton */__webpack_require__.d(__webpack_exports__, "IconButton", function() { return IconButton; });
/* concated harmony reexport Checkbox */__webpack_require__.d(__webpack_exports__, "Checkbox", function() { return Checkbox; });
/* concated harmony reexport CommandBar */__webpack_require__.d(__webpack_exports__, "CommandBar", function() { return CommandBar; });
/* concated harmony reexport ContextualMenu */__webpack_require__.d(__webpack_exports__, "ContextualMenu", function() { return ContextualMenu; });
/* concated harmony reexport Dropdown */__webpack_require__.d(__webpack_exports__, "Dropdown", function() { return Dropdown; });
/* concated harmony reexport Icon */__webpack_require__.d(__webpack_exports__, "Icon", function() { return Icon; });
/* concated harmony reexport Label */__webpack_require__.d(__webpack_exports__, "Label", function() { return Label; });
/* concated harmony reexport Link */__webpack_require__.d(__webpack_exports__, "Link", function() { return Link; });
/* concated harmony reexport Persona */__webpack_require__.d(__webpack_exports__, "Persona", function() { return Persona; });
/* concated harmony reexport OverflowSet */__webpack_require__.d(__webpack_exports__, "OverflowSet", function() { return OverflowSet; });
/* concated harmony reexport ProgressIndicator */__webpack_require__.d(__webpack_exports__, "ProgressIndicator", function() { return ProgressIndicator; });
/* concated harmony reexport SearchBox */__webpack_require__.d(__webpack_exports__, "SearchBox", function() { return SearchBox; });
/* concated harmony reexport Shimmer */__webpack_require__.d(__webpack_exports__, "Shimmer", function() { return Shimmer; });
/* concated harmony reexport Spinner */__webpack_require__.d(__webpack_exports__, "Spinner", function() { return Spinner; });
/* concated harmony reexport Slider */__webpack_require__.d(__webpack_exports__, "Slider", function() { return Slider; });
/* concated harmony reexport TextField */__webpack_require__.d(__webpack_exports__, "TextField", function() { return TextField; });
/* concated harmony reexport Toggle */__webpack_require__.d(__webpack_exports__, "Toggle", function() { return Toggle; });




/***/ }),

/***/ "fc6b":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextField_vue_vue_type_style_index_0_id_b28a83c4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("c31a");
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextField_vue_vue_type_style_index_0_id_b28a83c4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextField_vue_vue_type_style_index_0_id_b28a83c4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */
 /* unused harmony default export */ var _unused_webpack_default_export = (_node_modules_vue_style_loader_index_js_ref_8_oneOf_1_0_node_modules_css_loader_index_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_lib_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_lib_loader_js_ref_8_oneOf_1_4_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TextField_vue_vue_type_style_index_0_id_b28a83c4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "fdef":
/***/ (function(module, exports) {

module.exports = '\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003' +
  '\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF';


/***/ })

/******/ });
});
//# sourceMappingURL=office-ui-fabric-vue.umd.js.map